from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
import csv

# idiom year meaning examples

driver = webdriver.Chrome(executable_path="/Users/kevincheng/selenium driver/chromedriver")
driver.get("https://hkdic.my-helper.com/")
main_window_handle = driver.current_window_handle

action = ActionChains(driver)
yearElements = driver.find_elements_by_css_selector(".year")
for i in range(len(yearElements)):
    wordElements = driver.find_elements_by_css_selector(f'.year:nth-child({i + 1}) a.word')
    year = driver.find_element_by_css_selector(f".year:nth-child({i + 1}) h1").text
    for j in range(len(wordElements)):
        # len(wordElements)
        newWordElements = driver.find_elements_by_css_selector(f'.year:nth-child({i + 1}) a.word')[j]
        text = newWordElements.text
        # 男神 is deleted in the site
        if text == '男神':
            continue
        newWordElements.click()
        wait = WebDriverWait(driver, 30)
        wait.until(expected_conditions.presence_of_element_located((By.CSS_SELECTOR, '.breadcrumb-item a')))
        meaning = driver.find_element_by_css_selector('.m-0').text
        example = driver.find_element_by_css_selector('.margin-0').text
        driver.find_element_by_css_selector('.navbar-brand').click()
        # dismiss-button
        # string = f'{wordElements[j].text, year}'
        newData = [[text, year, meaning, example]]
        myFile = open('/Users/kevincheng/tecky course/pj1/db/chiuyu/chiuyudictionary.csv', 'a+')
        with myFile:
            writer = csv.writer(myFile)
            writer.writerows(newData)
        # data.append([text, year, meaning, example])
        # print(f'{wordElements[j].text} {year}')
        # print(data)







# wordElement = driver.find_element_by_css_selector("a.word")
# action.move_to_element(wordElement).click().perform()

# with open('/Users/kevincheng/tecky course/pj1/chiuyu/chiuyudictionary.csv') as csvDataFile:
#     csvReader = csv.DictReader(csvDataFile)
#     for row in csvReader:
#         print(row['Name'], "--", row['Description'])
#
# myData = [["Name","Product","Description"], ["karthiq32321", "BE", "Mechanical"], ['Raj231232', 'BTech', 'Computers']]
# myFile = open('/Users/kevincheng/tecky course/pj1/chiuyu/chiuyudictionary.csv', 'a+')
# with myFile:
#     writer = csv.writer(myFile)
#     writer.writerows(myData)

# child_menu = driver.find_element_by_link_text("Top")
# action.move_to_element(child_menu).click().perform()
#
# print(driver.find_element_by_css_selector("#displayed-text").is_displayed())
#
# driver.find_element_by_css_selector("#hide-textbox").click()
#
# print(driver.find_element_by_css_selector("#displayed-text").is_displayed())
#
# driver.find_element_by_css_selector("#name").send_keys("hhhello")
# print(driver.find_element_by_css_selector("#name").get_attribute("value"))
# print(driver.execute_script("return document.getElementById('name').value"))
#
# home_button = driver.find_element_by_css_selector("button")
# driver.execute_script("window.scrollTo(0,document.body.scrollHeight);")
#
# driver.execute_script("arguments[0].click();", home_button)
#
#
# driver.get("https://chercher.tech/practice/practice-pop-ups-selenium-webdriver")
# child_window_handle = driver.window_handles[0]
# # size = len(child_window_handle)
#
# # for x in range(size):
# #     driver.switch_to.window(child_window_handle[x])
# #     print(driver.title)
# #
# driver.switch_to.window(child_window_handle)
# child_action = ActionChains(driver)
# # driver.find_element_by_css_selector("[name=alert]").click()
# child_action.double_click(driver.find_element_by_css_selector("#double-click")).perform()
#
# alert = driver.switch_to.alert
# assert "You double clicked me!!!, You got to be kidding me" == alert.text
# alert.accept()
