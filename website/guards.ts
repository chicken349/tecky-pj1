import { Request, Response, NextFunction } from 'express'

export function isLoggedIn(req: Request, res: Response, next: NextFunction) {
  if (req.session?.['user']) {
    console.log("logged in")
    next()
  } else {
    res.redirect('/connect/google')
  }
}
