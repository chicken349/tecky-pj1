import express, {Request, Response} from 'express'
import path from 'path'
import { db } from './db'
// import { checkPassword, hashPassword } from './hash'
import fetch from 'node-fetch'


export let userRoute = express.Router()

userRoute.get('/summary/api', summariseMemos)
userRoute.get('/summary', genSummary)
userRoute.get('/group', sendTypesHTML)
userRoute.get('/group/api', searchMemoTypeOrYear)
userRoute.get('/search', showIdioms)
userRoute.get('/search/:id', searchMemos)
userRoute.post('/summary', (req, res) => res.redirect('/summary'))
userRoute.post('/new-word', addNewWord)
userRoute.get('/chat', chat)
userRoute.get('/role', getRole)
userRoute.get('/user-detail', getUserDetail)
userRoute.get('/select-options', getSelectOptions)
userRoute.get('/check-username-exist', checkUsernameExist)
userRoute.post('/user/user-profile', userProfile)
userRoute.get('/user/user-profile', (req, res) => res.sendFile(path.join(__dirname, "protected", "user-profile.html")))
userRoute.get('/colorPref', colorPref)
userRoute.get('/update-color-mode', setColorModeToSession)
userRoute.post('/fav-list', updateFavList)
userRoute.get('/fav-list', getFavList)
userRoute.get('/search-word', searchWordByKey)
userRoute.get('/fav-list-session', getFavListSession)


async function querySituationTypes() {
  return (await db.query(/* sql */`SELECT id, type from situation_types`)).rows.sort(() => Math.random() - 0.5)
}

async function summariseMemos(req: Request, res: Response) {
  let sampleIdiomsSelected = [];

  try {
    let sampleIdioms = (await db.query(/* sql */`SELECT idioms.id,idioms.key, meaning from meanings inner join idioms on idiom_id = idioms.id`)).rows
    let randomNumbers: number[] = []
    for (let i = 0; i < 10; i++) {
      let randomNumber = Math.floor(Math.random() * sampleIdioms.length)
      while (randomNumbers.includes(randomNumber)) {
        randomNumber = Math.floor(Math.random() * sampleIdioms.length)
      }
      randomNumbers.push(randomNumber)
      sampleIdiomsSelected.push(sampleIdioms[randomNumber])
    }

    let idiomTypes = await querySituationTypes()
    // console.log(sampleIdiomsSelected)
    res.json({sampleIdiomsSelected, idiomTypes})
  } catch(e) {
    console.log(e)
    res.end("error")
  }
}

async function genSummary(req: Request, res: Response) {

  res.sendFile(path.join(__dirname, 'public','summary.html'))

}

async function searchMemos(req: Request, res: Response) {
  let id = req.params['id']
  let idiomDetails = {}
  console.log(id)
  try {
    let meanings = (await db.query(/* sql */`SELECT idioms.key, meaning from meanings inner join idioms on idioms.id = $1 and idiom_id = $1`,[id])).rows
    console.log(meanings)
    let idiom_situations = (await db.query(/* sql */`SELECT type from idiom_situations inner join idioms on idioms.id = $1 and idiom_id = $1 inner join situation_types on situation_types_id = situation_types.id`,[id])).rows
    console.log(idiom_situations)
    let examples = (await db.query(/* sql */`SELECT example from examples inner join idioms on idioms.id = $1 and idiom_id = $1`,[id])).rows
    console.log(examples)
    let initiate_words = (await db.query(/* sql */`SELECT initiate_words from initiate_words inner join idioms on idioms.id = $1 and idiom_id = $1`,[id])).rows
    console.log(initiate_words)
    idiomDetails['meanings'] = meanings
    idiomDetails['idiom_situations'] = idiom_situations
    idiomDetails['examples'] = examples
    idiomDetails['initiate_words'] = initiate_words
    let idiomTypes = await querySituationTypes()

    res.json({idiomDetails, idiomTypes})
  } catch(e) {
    console.log(e)
    res.end("error")
  }
}

async function searchMemoTypeOrYear(req: Request, res: Response) {
  let id = req.query['id']
  let year = req.query['input']
  let queryByRequest;

  try {
    if (id) {
      queryByRequest = (await db.query(/* sql */`SELECT idioms.key, idioms.id from idioms inner join idiom_situations on situation_types_id = $1 and idioms.id = idiom_id`,[id])).rows.sort(() => Math.random() - 0.5)
    } else {
      queryByRequest = (await db.query(/* sql */`SELECT idioms.key, idioms.id from idioms where year_popular = $1`,[year])).rows.sort(() => Math.random() - 0.5)
    }
    
    let idiomTypes = await querySituationTypes()
    console.log("queryByRequest",queryByRequest)
    res.json({queryByRequest, idiomTypes})

  } catch(e) {
    console.log(e)
    res.end("error")
  }
}

type newWord = {
  key: string
  type: string
  example: string
  meaning: string
}

async function addNewWord(req: Request, res: Response) {
  let key: string = req.body?.key
  let type: string = req.body?.type
  let example: string = req.body?.example
  let meaning: string = req.body?.meaning

  let content = {key,type,example,meaning}
  console.log(content)
  
  insert(content)
    .then(() => res.status(201).end("created"))
    .catch(() => res.status(500).end('Failed to save memos'))
}

async function insert(memoContent:newWord) {
  let content = memoContent

  try {
    if (!content?.meaning || !content?.example || !content?.key || !content?.type) {
      throw Error('Missing contents. Please retry.')
    }

    await db.query(/* sql */`
      INSERT into idioms (key,year_popular,idiom_type,created_at,updated_at) VALUES ($1, $2, $3, NOW(), NOW())
    `, [content.key, new Date().getFullYear(), "潮語"])

    let idiom_id = (await db.query(`SELECT id from idioms where key = $1`,[content.key])).rows[0].id
      // console.log(idiom_id)
    let user_id = (await db.query(`SELECT id from users where username = $1`,["admin1"])).rows[0].id
    await db.query(/* sql */`
    INSERT into meanings (idiom_id, user_id, meaning) values ($1, $2, $3)
    `,[idiom_id, user_id, content.meaning])

    let situation_types_id = (await db.query(`SELECT id from situation_types where type = $1`,[content.type])).rows[0].id;
    await db.query(/* sql */ `
    insert into idiom_situations (idiom_id, situation_types_id) values ($1, $2)
    `, [idiom_id, situation_types_id])

    await db.query(
      'insert into examples (idiom_id, example) values ($1, $2)',
      [idiom_id, content.example])

    await db.query(
      'insert into initiate_words (idiom_id, initiate_words) values ($1, $2)',
      [idiom_id, content.key])

    await db.query(
      'insert into origins (idiom_id, origin) values ($1, $2)',
      [idiom_id, content.meaning])

  } catch(e) {
    console.log(e)
    throw Error('Please retry.')
  }
}

function sendTypesHTML(req: Request, res: Response) {
  res.sendFile(path.join(__dirname, 'public','groups.html'))
}

function showIdioms(req: Request, res: Response) {
  res.sendFile(path.join(__dirname, 'public','search.html'))
}

function chat(req: Request, res: Response) {
  res.sendFile(path.join(__dirname, 'public','chathome.html'))
}

async function getSelectOptions(req: Request, res: Response) {
  try {
    let selectOptions = (await db.query(/* sql */`SELECT type from situation_types`)).rows

    // console.log("selectOptions",selectOptions)
    res.json({selectOptions})

  } catch(e) {
    console.log(e)
    res.end("error")
  }
}




userRoute.get('/login/google', loginGoogle)
// userRoute.post('/login', login)
userRoute.get('/logout', logout)
// userRoute.post('/register', register)

type GoogleResult = {
  id: string
  email: string
  verified_email: boolean
  name: string
  given_name: string
  family_name: string
  picture: string
  locale: string
  hd: string
}

async function loginGoogle(req: express.Request, res: express.Response) {
  const accessToken = req.session?.['grant'].response.access_token
  const fetchRes = await fetch(
    'https://www.googleapis.com/oauth2/v2/userinfo',
    {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    },
  )
  const fetchResult: GoogleResult = await fetchRes.json()
  console.log("google",fetchResult.picture)
  let dbResult = await db.query(`select * from users where email = $1`, [
    fetchResult.email,
  ])
  let user = dbResult.rows[0]

  // if this user is new comer
  if (!user) {
    dbResult = await db.query(
      `insert into users (email, google_token, profile_pic, is_admin, is_banned, created_at, updated_at, dark_mode) values ($1, $2, $3, false, false, now(), now(), true) returning *`,
      [fetchResult.email, accessToken, fetchResult.picture],
    )
    req.session['user'] = dbResult.rows[0]
    res.redirect('/')
    return
  }

  // if this user already registered
  await db.query(`update users set google_token = $1 where id = $2`, [
    accessToken,
    user.id,
  ])
  user.google_token = accessToken
  req.session['user'] = user
  res.redirect('/')
}

async function logout(req: express.Request, res: express.Response) {
  req.session['user'] = false;  
  res.redirect('/')
}

function getRole(req: Request, res: Response) {
  console.log('user',req.session['user'])
  if (!req.session || !req.session['user']) {
    res.end('guest')
    return
  }
  if (req.session['user'].is_admin) {
    res.end('admin')
    return
  }
  res.end('member')
}

async function getUserDetail(req: express.Request, res: express.Response) {
  console.log('user',req.session['user'])

  res.json(req.session['user'])
}


async function userProfile(req: Request, res: Response) {
  console.log("userprofile")
  let username: string = req.body?.username
  console.log("username",username)
  let email: string = req.body?.email
  let colorPreference: boolean = req.body?.colorPreference == "Light" ? false : true

  try {
    await db.query(
      `update users set username = $1, dark_mode = $2 where email = $3`,[username, colorPreference ,email])
      
      req.session['user'].username = username
      req.session['user'].dark_mode = colorPreference
      res.status(201).end("updated")

  } catch(e) {
    console.log(e)
    res.status(500).end('Failed')
  }
}

async function checkUsernameExist(req: Request, res: Response) {
  let username = req.query.username
  console.log(username)


  try {
    let usernameReturned = (await db.query(/* sql */`SELECT username, email from users where username = $1 or email = $1`,[username])).rows[0]
    if (!usernameReturned) {
      res.json({user: ""})
    }
    console.log({usernameReturned})

    // console.log("selectOptions",selectOptions)
    res.json({user: usernameReturned})

  } catch(e) {
    console.log(e)
    res.end("error")
  }
}

async function colorPref(req: Request, res: Response) {
  let colorPreference: boolean = req.query?.color == "Light" ? false : true
  let email = req.session['user'].email
  console.log(req.query?.color)

  try {
    await db.query(
      `update users set dark_mode = $1 where email = $2`,[colorPreference, email])
      req.session['user'].dark_mode = colorPreference
      res.status(201).end("updated")

  } catch(e) {
    console.log(e)
    res.status(500).end('Failed')
  }
}

async function setColorModeToSession(req: Request, res: Response) {
  let colorPref = req.query?.color
  console.log("colorPref",colorPref)
  console.log(req.session)
  console.log(req.session?.['color'])

  if (colorPref) {
    req.session['color'] = colorPref
    res.end('success')
  } else {
    res.json(req.session?.['color'])
  }
}

async function updateFavList(req: Request, res: Response) {
  let keyId: string = req.body?.keyId
  console.log("keyId",keyId)

  if (!req.session['favList']) {
    req.session['favList'] = [keyId]
  } else {
    req.session['favList'].push(keyId)
  }

  if (req.session['user']) {
    try {
      let userEmail = req.session['user'].email

      let userFavedWordWithKeyId = (await db.query(/* sql */ `
        SELECT idiom_id from user_fav_words where idiom_id = $1
      `,[keyId])).rows

      if (userFavedWordWithKeyId.length === 0) {
        await db.query(/* sql */ `
        INSERT INTO user_fav_words (user_id, idiom_id) VALUES ((SELECT id from users where email = $1), $2)
      `,[userEmail, keyId])
      }
      // req.session['favList'] = null

      res.status(201).end('updatedUserFavList')
    } catch (e) {
      console.log(e)
      res.status(500).end('error in updateUserFavList')
    }
  }
  res.status(201).end('saved favlist in session')

}

async function getFavList(req: Request, res: Response) {
  let email = req.query?.email
  console.log("user email",email)

  try {
    let favWords = (await db.query(/* sql */`
    SELECT key from idioms where idioms.id in (SELECT idiom_id from user_fav_words where user_fav_words.user_id in (SELECT users.id from users where users.email = $1))
    `,[email])).rows
    
    console.log("favWords",favWords)
    res.json(favWords)
  } catch (e) {
    console.log(e)
    res.end('error')
  }

}

async function searchWordByKey(req: Request, res: Response) {
  let key = req.query?.key
  console.log("key from search",key)

  try {
    let queryId = (await db.query(/* sql */`
      SELECT id from idioms where key = $1
    `,[key])).rows[0]
    if (!queryId) {
      queryId = (await db.query(/* sql */`
      SELECT id from idioms where key iLike $1
    `,['%' + key + '%'])).rows[0]
    }
    if (!queryId) {
      let queryIds = (await db.query(/* sql */`
      SELECT id from idioms
    `)).rows
      queryId = queryIds[Math.floor(Math.random() * queryIds.length)]
    }
    console.log("queryId",queryId)
    res.json(queryId)
  } catch (e) {
    console.log(e)
    res.end('error')
  }
}

async function getFavListSession(req: Request, res: Response) {
  if (req.session?.['favList']) {
    res.json(req.session['favList'])
  } else {
    res.json({error: "error"})
  }
  
}
