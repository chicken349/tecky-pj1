// only create tables once!!!!
// Not use in production!!

import { Client } from "pg";
import dotenv from "dotenv";

dotenv.config();

export const db = new Client({
  database: process.env.DB_NAME,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
});

db.connect()
  .then(() => console.log("connected now"))
  .catch((err) => console.log(err));

db.query(
  /* sql */`
        CREATE TABLE users(
          id SERIAL primary key,
          username VARCHAR(20) unique,
          email VARCHAR(255) not null unique,
          profile_pic text,
          is_admin Boolean not null,
          is_banned Boolean not null,
          created_at timestamp not null,
          updated_at timestamp not null,
          google_token VARCHAR(255),
          dark_mode Boolean not null
      );
      CREATE TABLE user_fav_words(
        id SERIAL primary key,
        user_id integer not null,
        FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
        idiom_id integer not null,
        FOREIGN KEY (idiom_id) REFERENCES idioms(id) ON DELETE CASCADE
      );
      CREATE TABLE idiom_types(
          id SERIAL primary key,
          type VARCHAR(255) not null unique
      );
      CREATE TABLE situation_types(
          id SERIAL primary key,
          type VARCHAR(255) not null unique
      );
      CREATE TABLE idioms(
          id SERIAL primary key,
          key VARCHAR(255) unique not null,
          year_popular integer not null,
          idiom_type VARCHAR(255) not null,
          count integer not null default 0,
          FOREIGN KEY (idiom_type) REFERENCES idiom_types(type),
          created_at timestamp not null,
          updated_at timestamp not null
      );
      CREATE TABLE meanings(
          id SERIAL primary key,
          idiom_id integer not null,
          FOREIGN KEY (idiom_id) REFERENCES idioms(id) ON DELETE CASCADE,
          user_id integer not null,
          FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
          meaning text not null
      );
      CREATE TABLE idiom_situations(
          id SERIAL primary key,
          idiom_id integer not null,
          FOREIGN KEY (idiom_id) REFERENCES idioms(id) ON DELETE CASCADE,
          situation_types_id integer not null,
          FOREIGN KEY (situation_types_id) REFERENCES situation_types(id) ON DELETE CASCADE
      );
      CREATE TABLE examples(
          id SERIAL primary key,
          idiom_id integer not null,
          FOREIGN KEY (idiom_id) REFERENCES idioms(id) ON DELETE CASCADE,
          example VARCHAR(500) not null
      );
      CREATE TABLE initiate_words(
          id SERIAL primary key,
          idiom_id integer not null,
          FOREIGN KEY (idiom_id) REFERENCES idioms(id) ON DELETE CASCADE,
          initiate_words VARCHAR(30) not null
      );
      CREATE TABLE chatroom_data(
        id SERIAL primary key,
        message json not null,
        room text not null
      );
      `
)
  .then(() => {
    console.log("added tables");
    db.end();
  })
  .catch((e) => {
    console.log(e);
  });
