import express, {Request, Response} from 'express'
import { db } from './db'
// import path from 'path'
// import fs from 'fs'

export let idiomSuggestRoute = express.Router()

idiomSuggestRoute.get('/words/search', suggestIdioms)
idiomSuggestRoute.post('/selected-word', updateSelectDB)
idiomSuggestRoute.get('/meaning', getMeaning)

async function suggestIdioms(req: Request, res: Response) {
  // const words = (await db.query(/* sql */`SELECT key from idioms`)).rows

  try {
    let favWords;
    console.log(req.session['user'])
    if (req.session['user']) {
      let email = req.session['user'].email
      favWords = (await db.query(/* sql */`
      SELECT key from idioms where idioms.id in (SELECT idiom_id from user_fav_words where user_fav_words.user_id in (SELECT users.id from users where users.email = $1))
      `,[email])).rows
      console.log("favWords",favWords)
    }

    let q = req.query?.q as string
    console.log("mobile",q)    
    for (;;) {
      if (!q) {
        res.json([])
        return
      }
      let matched = (await db.query(/* sql */`
      SELECT key from idioms where idioms.id IN ( SELECT idiom_id from initiate_words where initiate_words iLIKE $1) ORDER BY count DESC limit 20
      `,[q + '%'])).rows
      if (matched.length === 0) {
        matched = (await db.query(/* sql */`
        SELECT key from idioms where idioms.id IN ( SELECT idiom_id from initiate_words where initiate_words iLIKE $1) ORDER BY count DESC limit 20
        `,['%' + q + '%'])).rows
      }
      if (matched.length > 0) {
        let words = matched.map((word) => word.key)
        console.log("Towords",words)
        if (favWords && favWords.length > 0) {
          favWords.forEach(favWord => {
            console.log(favWord.key)
            if (words.indexOf(favWord.key) !== -1) {
              words.filter(word => word !== favWord.key)
              words.unshift(favWord.key)
            }
          })
        }
        console.log({q, words})
        res.json({ q, words })
        return
      }
      q = q.substring(1)
    }
  } catch (e) {
    console.log(e)
    res.end('error')
  }
}

async function updateSelectDB(req: Request, res: Response) {
  let word = req.body?.selectedWord
  insertToDB(word)
    .then(() => res.status(201).end("updated"))
    .catch(() => res.status(501).end("failed"))

  async function insertToDB(word: any) {
    console.log(word)
    try {
      await db.query(/* sql */`
      update idioms set count = count + 1 where key = $1
    `, [word])
    } catch (e) {
      console.log(e)
      res.end('error')
    }
  }
}

async function getMeaning(req: Request, res: Response) {
  let word = req.query?.word
  console.log("getMeaning", word)
  try {
    let meaning = (await db.query(/* sql */`
      SELECT meaning from meanings where idiom_id = (SELECT id from idioms where key = $1)
    `, [word])).rows
    console.log("meaningQ", meaning)
    res.json(meaning)
  } catch (e) {
    console.log("meaningE", e)
    res.json([{error: e}])
  }
}