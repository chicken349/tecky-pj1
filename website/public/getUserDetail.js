async function checkUserDetail() {
  let res = await fetch('/user-detail')

  try {
    let userDetail = await res.json()
    console.log("userDetail",userDetail)
    return userDetail
  } catch(e) {
    console.log(e)
    return
  }
}