// document.querySelector("user-profile")
    // .addEventListener('submit', 
    
let usernameInput = document.querySelector("#username")

showUsers()

async function showUsers() {
    const json = await checkUserDetail()
    if (!json) {
        return
    }
    console.log(json)
    
    updateUserInfo(json)
}

async function updateUserInfo(result) {
    console.log("result",result)
    if (result.is_admin) {
        document.querySelector("#role").innerHTML = "Admin"
    } else {
        document.querySelector("#role").innerHTML = "Member"
    }
    if (!result.dark_mode) {
        document.querySelector("#colorPreference").innerHTML = `
    <option selected value="Light">Light</option>
    <option value="Dark">Dark</option>
    `
    } else {
        document.querySelector("#colorPreference").innerHTML = `
        <option value="Light">Light</option>
        <option selected value="Dark">Dark</option>
        `
    }
    usernameInput.value = result.username ? result.username : ""
    document.querySelector("#email").innerHTML = result.email
    
    let userFavWords = await (await fetch(`/fav-list?email=${result.email}`)).json()
    let favWordsContainer = document.querySelector('.fav-words-container .scroll-bar-layer')
    userFavWords.forEach(userFavWord => favWordsContainer.innerHTML += `
        <div class="fav-words">${userFavWord.key}</div>
    `)
    // await (await fetch(`/search-word?key=${searchForm.search.value}`)).json()
}

// function updateUserInfo(result) {
//     document.querySelector("#username").innerHTML = result[0].username
// }

    // function appendData(data) {
    //     var mainContainer = document.getElementById("myData");
    //     for (var i = 0; i < data.length; i++) {
    //         var div = document.createElement("div");
    //         div.innerHTML = 'Name: ' + data[i].firstName + ' ' + data[i].lastName;
    //         mainContainer.appendChild(div);


// document.querySelector("#username").value

let userProfileForm = document.querySelector("#user-profile-form")

userProfileForm.addEventListener('submit', submitHandle)

async function submitHandle(event) {
	event.preventDefault()

    let email = document.querySelector("#email").textContent
    let role = document.querySelector("#role").textContent

    console.log("formname",userProfileForm.username.value)
	let res = await fetch('/user/user-profile', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
        // refer to ts later
		body: JSON.stringify({    
			username: userProfileForm.username.value,
			email: email,
			role: role,
			colorPreference: userProfileForm.colorPreference.value,
		}),
	})
	let message = await res.text()
	console.log(message)
	if (res.status === 201) {
		console.log(res)
        const userDetail = await checkUserDetail()
        usernameInput.value = userDetail.username
        window.location = "/user/user-profile"
	} else {
		alert(message)
	}
}



