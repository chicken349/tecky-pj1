let labels = document.querySelectorAll('.icon-labels')
let titleWords = document.querySelectorAll('.title-word')
let searchForm = document.querySelector("#search-form")
let searchInput = document.querySelector("#search")
let homeSearchList = document.querySelector(".homepage-search-list")
let titleContainer = document.querySelector(".title-container")
let bottomContainer = document.querySelector(".bottom-container")
let header = document.querySelector(".header")
let isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)

for (let label of labels) {
  let angle = Math.floor(Math.random() * 12) - 5
  label.style.setProperty("--rotate", `rotate(${angle <= 0 ? angle - 1: angle}deg)`)
} 

for (let titleWord of titleWords) {
  let angle = Math.floor(Math.random() * 12) - 5
  titleWord.style.setProperty("--rotate", `rotate(${angle <= 0 ? angle - 1: angle}deg)`)
}

searchForm.addEventListener('submit', async () => {
  let searchResult = await (await fetch(`/search-word?key=${searchForm.search.value}`)).json()
  window.location = `/search?id=${searchResult.id}`
})

searchInput.addEventListener('input', () => scheduleLoadListToSearch(searchInput, homeSearchList, 'home-search-word'))
addClickEventListenerRemoveSearchList(titleContainer, searchInput, homeSearchList)
addClickEventListenerRemoveSearchList(bottomContainer, searchInput, homeSearchList)
addClickEventListenerRemoveSearchList(header, searchInput, homeSearchList)


getUserProfile()
// console.log(document.querySelector('.toggle-up'))

async function getUserProfile() {



    let colorBtn = document.querySelector('.toggle-up')
    checkDarkMode(colorBtn)
  

}

