function addClickEventListenerRemoveSearchList(element, input, list) {
  element.addEventListener('click', () => {
    
    console.log(element)
    input.value = ''
    list.innerHTML = ''
    list.style.visibility = "hidden"
  })
}

function addClickEventListenerRemoveSearchListAndClose(element, input, list, form) {
  element.addEventListener('click', () => {
    
    console.log(element)
    input.value = ''
    input.style.display = 'none'
    list.innerHTML = ''
    list.style.visibility = "hidden"
    form.style.width = "50px"
  })
}



let loadListTimerToSearch;
function scheduleLoadListToSearch(input, list, className) {
  clearTimeout(loadListTimerToSearch)
  loadListTimerToSearch = setTimeout(() => loadListToSearch(input, list, className), 500)
}
async function loadListToSearch(input, list, className) {
  // let input = document.querySelector('#new-message')
  let text = input.value
  // console.log(text)
  if (!text) {
    input.value = ''
    list.innerHTML = ''
    list.style.visibility = "hidden"
    return
  }
  let res = await fetch('/words/search?q=' + text)
  let json = await res.json()
  // console.log("json",json)
  let q = json.q
  let words = json.words || []
  list.innerHTML = ''
  words.forEach(word => {
    let div = document.createElement('div')
    div.className = className
    // div.id = word
    div.textContent = word
    div.onmousedown = () => takeSuggestionToSearch(word, input, q)
    list.appendChild(div)
  })
  const searchResultContainer = document.querySelector('.search-response-list-container')
  const chatHeader = document.querySelector('.chat-header')
  if (searchResultContainer && chatHeader) {
    let chatHeaderY = chatHeader.getBoundingClientRect().bottom
    searchResultContainer.style.top = chatHeaderY.toString() + "px"
  }
  
  list.style.visibility = "visible"

}


async function takeSuggestionToSearch(word, input, q) {
  // let text = input.value
  console.log('takeSuggestion', { q, word })

  input.value = word
  q = word
  
  let res = await fetch('/selected-word', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      selectedWord: word,
    }),
  })
  // console.log("word",word)
  let message = await res.text()
  // console.log(message)

  
  if (!isMobile) {
    // false for not mobile device
    console.log("inputfocus")
    input.focus()
    input.setSelectionRange(input.value.length, input.value.length)
  }
}