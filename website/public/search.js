let sectionTypes = ['搜尋結果', '潮語分類', '潮語年份']

checkDarkMode()
toggleAddBtnZIndex()
toggleSection(sectionTypes)
loadMeanings()

let type_table = document.querySelector('#type_table')
let year_table = document.querySelector('#year_table')
let thisYear = new Date().getFullYear()

let keyEl = document.querySelector('#idioms .key')
let meaningsUl = document.querySelector('#idioms .meanings')
let situationsUl = document.querySelector('#idioms .idioms-situation')
let examplesUl = document.querySelector('#idioms .examples')

async function loadMeanings() {
  try {
    let params = new URLSearchParams(location.search)
    let id = params.get('id')

    let res = await fetch(`/search/${id}`)
    let resFormatted = await res.json()
    let details = resFormatted.idiomDetails
    let idiomTypes = resFormatted.idiomTypes
    let favStar = document.querySelector('.fav-star')
    console.log(details)
    
    keyEl.id = 'key' + id
    keyEl.innerHTML = `<h3>${details.meanings[0].key}</h3>`

    // console.log(details[3])
    for (let meaning of details.meanings) {
      meaningsUl.innerHTML += `
        <li class="box-shadow-list">${meaning.meaning}</li>
      `
    }

    for (let situation of details.idiom_situations) {
      situationsUl.innerHTML += `
        <li class="inline-list">${situation.type}</li>
      `
    }

    console.log(details.examples)
    for (let i = 0; i < details.examples.length; i++) {
      examplesUl.innerHTML += `
        <li class="box-shadow-list">例子${i + 1}: ${details.examples[i].example}</li>
      `
    }

    favStar.addEventListener('click', addFav)

    async function addFav() {
      try {
        let res = await fetch('/fav-list', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            keyId: id,
          }),
        })
        console.log("word",id)
        let message = await res.text()
        console.log(message)
        return res.status
      } catch (e) {
        console.log(e)
      }
    
    }

    updateTypeTable(idiomTypes)
    
    updateYearTable(thisYear)

    animateFirstTime()

  } catch (e) {
    console.log(e)
  }
}

