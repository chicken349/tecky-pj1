let joinForm = document.querySelector("#join-chatroom")
let usernameInput = document.querySelector("#username")
let userDetailReceived;

presetHome()

async function presetHome() {
  await checkUserDetails()

  joinForm.addEventListener('submit', checkUserName)
}


async function checkUserName(e) {
  e.preventDefault()
  let username = usernameInput.value

  if (!username) {
    alert("Please enter username.")
    return
  }
  
  let res = await fetch(`/check-username-exist?username=${usernameInput.value}`)
  let json = await res.json()
  console.log(json)

  let usernameReceived = json.user.username || null
  let emailReceived = json.user.email || null
  console.log(usernameReceived, emailReceived)
  console.log(userDetailReceived?.email)
  
  if (!usernameReceived && !emailReceived) {
    console.log("no duplicate user")
    window.location = `/chatroom?username=${username}&room=${joinForm.room.value}`
    return
  }
  if ((usernameReceived && !userDetailReceived) || (emailReceived && !userDetailReceived)) {
    alert("There is user registered. Please choose another name.")
    return
  }
  if ((usernameReceived !== userDetailReceived?.username) && (emailReceived !== userDetailReceived?.email)) {
    alert("There is user registered. Please choose another name.")
    return
  }
  window.location = `/chatroom?username=${username}&room=${joinForm.room.value}`
  
}

async function checkUserDetails() {
  let userDetail;

  try {
    userDetail = await checkUserDetail()
  } catch (e) {
    console.log('not login')
    return
  }


  if (userDetail) {
    if (userDetail.username) {
      usernameInput.value = userDetail.username
    } else {
      usernameInput.value = userDetail.email
    }
  }

  userDetailReceived = userDetail
}

