let params = new URLSearchParams(location.search)
let id = params.get('id')
let input = params.get('input')
document.querySelector('.first-layer').innerHTML = `<p>${input}</p>`
console.log(input)
let sectionTypes = [input.slice(0,4), '潮語分類', '潮語年份']

checkDarkMode()
toggleAddBtnZIndex()
toggleSection(sectionTypes)
loadIdioms()

async function loadIdioms() {
  try {

    let result_table = document.querySelector('#result_table')
    let type_table = document.querySelector('#type_table')
    let year_table = document.querySelector('#year_table')
    let thisYear = new Date().getFullYear()
    let res = await fetch(`/group/api?input=${input}${id ? '&id=' + id : ""}`)
    let resFormatted = await res.json()
    console.log(resFormatted)
    let queryByRequest = resFormatted.queryByRequest
    let idiomTypes = resFormatted.idiomTypes

    updateResultTable(queryByRequest)

    updateTypeTable(idiomTypes)
    
    updateYearTable(thisYear)

    animateFirstTime()

  } catch (e) {
    console.log(e)
  }
}
