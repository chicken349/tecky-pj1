
// function checkKey(event) {
//   console.log(event.key)
//   switch (event.key) {
//     case 'ArrowUp':
//       document.activeElement.previousElementSibling.focus()
//       break
//     case 'ArrowDown':
//       document.activeElement.nextElementSibling.focus()
//       break
//     case 'Enter':
//       event.preventDefault()
//       document.querySelector("#chat-submit-btn").click()
//     default:
//       return
//   }
//   if (event.target.tagName === 'INPUT') {
//     event.preventDefault()
//   }
// }
  // list.innerHTML = ''

let list = document.querySelector('.suggestion-list')
let listContainer = document.querySelector('.suggestion-list-container')
let input = document.querySelector('#new-message')
let preview = document.querySelector('#preview')
// let closeBtn = document.querySelector('#close-btn')
let chatMain = document.querySelector('.chat-main')
let chatMessagesBox = document.querySelector('.chat-messages')
let chatSideBar = document.querySelector('.chat-sidebar')
let chatHeader = document.querySelector('.chat-header')
let chatContainer = document.querySelector('.chat-container')
let chatFormMsg = document.querySelector('.chat-form-and-preview-container')
let touchStartTime = 0;
// let Mobile = navigator.userAgent
let isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
// let isIphone = /iPhone/i.test(navigator.userAgent)

// if (isIphone) {
//   chatContainer.style.height = "97vh"
// }

// document.body.addEventListener('click', () => {
//   console.log('key') 
// })

addClickEventListener(chatMessagesBox)
addClickEventListener(chatSideBar)
addClickEventListener(chatHeader)

function addClickEventListener(element) {
  element.addEventListener('mousedown', () => {
    list.innerHTML = ''
    list.style.visibility = "hidden"
    // chatMain.style.overflowY = "auto"
    console.log(element)
    // closeBtn.style.visibility = "hidden"
    preview.innerHTML = ''
    let meaningEl = document.querySelector('.suggestion-word-meaning')
    if (meaningEl) {
      meaningEl.remove()
    }
  })
}

// input.addEventListener('click', () => {
// })


let loadListTimer;
function scheduleLoadList() {
  clearTimeout(loadListTimer)
  loadListTimer = setTimeout(loadList, 500)
}
let q = ''
let chatFormY = chatFormMsg.getBoundingClientRect().y
let bodyHeight = chatContainer.getBoundingClientRect().bottom
console.log(chatFormY, bodyHeight)
listContainer.style.bottom = (bodyHeight - chatFormY).toString() + "px"
async function loadList() {
  // let input = document.querySelector('#new-message')
  let text = input.value
  // console.log(text)
  if (!text) {
    list.innerHTML = ''
    preview.innerHTML = ''
    list.style.visibility = "hidden"
    // chatMain.style.overflowY = "auto"
    // closeBtn.style.visibility = "hidden"
    return
  }
  let res = await fetch('/words/search?q=' + text)
  let json = await res.json()
  // console.log("json",json)
  q = json.q
  let words = json.words || []
  list.innerHTML = ''
  words.forEach(word => {
    let div = document.createElement('div')
    div.className = 'suggestion-word'
    // div.id = word
    div.textContent = word
    div.onmousedown = () => takeSuggestion(word)
    div.onmouseenter = (e) => previewWord(e, word)
    div.onmouseleave = () => restoreOriginal()
    div.ontouchstart = (e) => previewWord(e, word)
    div.ontouchend = (e) => updateTouchWord(word, e)
    list.appendChild(div)
  })
  list.style.visibility = "visible"
  // chatMain.style.overflowY = "hidden"


  // if (list.innerHTML != '') {
  //   closeBtn.style.visibility = "visible"
  // }
  // console.log("now highlight")
  let regex = new RegExp(q + '$')
  let lettersToHighlight = text.search(regex)
  preview.innerHTML = text.slice(0,lettersToHighlight) + `<span class="input-highlight">${text.slice(lettersToHighlight)}</span>`
}
scheduleLoadList()

function previewWord(e, word) {
  console.log("preview",e.type)
  if (e.type == "touchstart") {
    // e.preventDefault()
    e.target.style.backgroundColor = "rgba(127, 255, 212, 0.4)"
    touchStartTime = new Date().getTime()

  }

  console.log('previewWord', { q, word })
  // console.log("input",input)
  let regex = new RegExp(q + '$')
  let lettersToHighlight = input.value.search(regex)
  preview.innerHTML = input.value.slice(0,lettersToHighlight) + `<span class="input-highlight">${word}</span>`

  previewMeaning(word, e).then().catch(e => console.log(e))
}

function restoreOriginal() {
  preview.innerHTML = input.value
}

function updateTouchWord(word, e) {
  e.preventDefault()
  let timePassed = new Date().getTime() - touchStartTime
  console.log({timePassed})
  e.target.style.backgroundColor = "rgba(44, 62, 80, 0.9)"

  if (timePassed >= 220) {
    console.log("over 220")
    restoreOriginal()
    return
  }
  console.log("under 220")
  takeSuggestion(word)

}

let currentSearchWord;
let lastSearchWord;
// let chatContainer = document.querySelector('.chat-container')

async function previewMeaning(word, e) {
  currentSearchWord = word

  if (currentSearchWord == lastSearchWord) {
    return
  }
  lastSearchWord = word

  let res = await fetch(`/meaning?word=${word}`)
  let meaning = (await res.json())[0]
  // console.log(meanings)

  let meaningDivs = document.querySelectorAll(".suggestion-word-meaning")
  if (meaningDivs.length > 0) {
    meaningDivs.forEach (meaningDiv => {
      meaningDiv.remove()
    })
  }

  // for (let meaning of meanings) {
  //   // console.log("meaning",meaning)
  if (meaning.meaning) {

    let wordTop = e.target.getBoundingClientRect().top
    bodyHeight = chatContainer.getBoundingClientRect().bottom
    console.log(bodyHeight, wordTop, bodyHeight-wordTop)
  
    let div = document.createElement('div')
    div.className = 'suggestion-word-meaning'
    div.textContent = meaning.meaning /* + wordTop + "px" + bodyHeight */
    div.style.bottom = (bodyHeight-wordTop).toString() + "px"
    // div.style.top = (wordTop).toString() + "px"
    chatContainer.appendChild(div)
  }
  // }

  // document.querySelector('.suggestion-word-meaning').style.bottom = (bodyHeight-wordTop + (isMobile ? 0 : 20)).toString() + "px"
}


async function takeSuggestion(word) {
  // let text = input.value
  console.log('takeSuggestion', { q, word })
  // console.log("input",input)
  // let input = document.querySelector('#new-message')
  let regex = new RegExp(q + '$')
  input.value = input.value.replace(regex, word)
  q = word


  let res = await fetch('/selected-word', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({
			selectedWord: word,
		}),
	})
  // console.log("word",word)
  let message = await res.text()
	// console.log(message)

  // highlight
  regex = new RegExp(q + '$')
  let lettersToHighlight = input.value.search(regex)
  // console.log("newregex",regex)
  // console.log("newsearch", lettersToHighlight)
  preview.innerHTML = input.value.slice(0,lettersToHighlight) + `<span class="input-highlight">${input.value.slice(lettersToHighlight)}</span>`

  let meaningEl = document.querySelector('.suggestion-word-meaning')
  if (meaningEl) {
    meaningEl.remove()
  }

  if (!isMobile) {
    // false for not mobile device
    console.log("inputfocus")
    input.focus()
    input.setSelectionRange(input.value.length, input.value.length)
  }

}





// function setInputPosition() {
//   let range = document.createRange()
//   let sel = window.getSelection()
  
//   range.setStart(input, 1)
//   range.collapse(true)
  
//   sel.removeAllRanges()
//   sel.addRange(range)

// }

// function getCaretIndex(element) {
//   let position = 0;
//   const isSupported = typeof window.getSelection !== "undefined";
//   if (isSupported) {
//     const selection = window.getSelection();
//     if (selection.rangeCount !== 0) {
//       const range = window.getSelection().getRangeAt(0);
//       const preCaretRange = range.cloneRange();
//       preCaretRange.selectNodeContents(element);
//       preCaretRange.setEnd(range.endContainer, range.endOffset);
//       position = preCaretRange.toString().length;
//     }
//   }
//   console.log("caret position", position)
//   return position;
// }