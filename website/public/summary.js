let cards = document.querySelectorAll(".card-item")
let readMorebtn = document.querySelector(".top-action")


// JavaScript Document
document.addEventListener("DOMContentLoaded", function() {

	checkDarkMode()

	toggleAddBtnZIndex()


	let sectionTypes = ['潮語推介', '分類', '年份']
	toggleSection(sectionTypes)

  async function loadIdioms() {
		try {
			let type_table = document.querySelector('#type_table')
			let year_table = document.querySelector('#year_table')
			let thisYear = new Date().getFullYear()
			let res = await fetch('/summary/api')
			let resFormatted = await res.json()
			console.log(resFormatted)
			let idiomSamples = resFormatted.sampleIdiomsSelected
			let idiomTypes = resFormatted.idiomTypes

			for (let i = 0; i < cards.length; i++) {
				// console.log(memo)
				cards[i].innerHTML = `
								<article class="word-container" id=key${idiomSamples[i].id}>
									<h1 class="word-title">${idiomSamples[i].key}</h3>
									<p class="word-content">${idiomSamples[i].meaning}</p>
								</article>
							`
			}

			updateTypeTable(idiomTypes)
			
			updateYearTable(thisYear)

		} catch (e) {
			console.log(e)
		}
		stackedCards();

		animateFirstTime()

  }
  loadIdioms()
  

	async function stackedCards () {

		let stackedOptions = 'Top'; //Change stacked cards view from 'Bottom', 'Top' or 'None'.
		let rotate = true; //Activate the elements' rotation for each move on stacked cards.
		let items = 3; //Number of visible elements when the stacked options are bottom or top.
		let elementsMargin = 10; //Define the distance of each element when the stacked options are bottom or top.
		let useOverlays = true; //Enable or disable the overlays for swipe elements.
		let maxElements; //Total of stacked cards on DOM.
		let currentPosition = 0; //Keep the position of active stacked card.
		let velocity = 0.3; //Minimum velocity allowed to trigger a swipe.
		let topObj; //Keep the swipe top properties.
		let rightObj; //Keep the swipe right properties.
		let leftObj; //Keep the swipe left properties.
		let listElNodesObj; //Keep the list of nodes from stacked cards.
		let listElNodesWidth; //Keep the stacked cards width.
    let listElNodesHeight;
		let currentElementObj; //Keep the stacked card element to swipe.
		let stackedCardsObj;
		let isFirstTime = true;
		let elementHeight;
		let obj;
		let elTrans;

		
		obj = document.getElementById('stacked-cards-block');
		stackedCardsObj = obj.querySelector('.stackedcards-container');
		listElNodesObj = stackedCardsObj.children;
		
		topObj = obj.querySelector('.stackedcards-overlay.top');
		rightObj = obj.querySelector('.stackedcards-overlay.right');
		leftObj = obj.querySelector('.stackedcards-overlay.left');

		
		countElements();
		currentElement();
		listElNodesWidth = stackedCardsObj.offsetWidth;
    listElNodesHeight = stackedCardsObj;
		currentElementObj = listElNodesObj[0];
		updateUi();

		//Prepare elements on DOM
		addMargin = elementsMargin * (items -1) + 'px';
		
		if(stackedOptions === "Top"){
	
			for(i = items; i < maxElements; i++){
				listElNodesObj[i].classList.add('stackedcards-top', 'stackedcards--animatable', 'stackedcards-origin-top');
			}
			
			elTrans = elementsMargin * (items - 1);
			
			stackedCardsObj.style.marginBottom = addMargin;
			
		} else if(stackedOptions === "Bottom"){
			
			
			for(i = items; i < maxElements; i++){
				listElNodesObj[i].classList.add('stackedcards-bottom', 'stackedcards--animatable', 'stackedcards-origin-bottom');
			}
			
			elTrans = 0;
			
			stackedCardsObj.style.marginBottom = addMargin;
			
		} else if (stackedOptions === "None"){
			
			for(i = items; i < maxElements; i++){
				listElNodesObj[i].classList.add('stackedcards-none', 'stackedcards--animatable');
			}
			
			elTrans = 0;
		
		}
			
		for(i = items; i < maxElements; i++){
			listElNodesObj[i].style.zIndex = 0;
			listElNodesObj[i].style.opacity = 0;
			listElNodesObj[i].style.webkitTransform ='scale(' + (1 - (items * 0.04)) +') translateX(0) translateY(' + elTrans + 'px) translateZ(0)';
			listElNodesObj[i].style.transform ='scale(' + (1 - (items * 0.04)) +') translateX(0) translateY(' + elTrans + 'px) translateZ(0)';
		}
		
		if(listElNodesObj[currentPosition]){
			listElNodesObj[currentPosition].classList.add('stackedcards-active');
			calculateTextOverflow(stackedCardsObj);
			function calculateTextOverflow(stackedCardsObj) {
				calculateLinesVisible()
		
				function calculateLinesVisible() {
					let contents = document.querySelectorAll('.word-content')
					
					let line = Math.max(Math.floor((stackedCardsObj.offsetHeight - 36 - 20 * 2) / 18) - 2,1)
					for (content of contents) {
						content.style.setProperty("--lineLimit", line)
					}
				}
		
				setInterval(calculateLinesVisible, 500)
			}

			readMorebtn.addEventListener('click', async () => {
				let id = document.querySelectorAll('.word-container')[currentPosition].id.slice(3)
				window.open(`/search?id=${id}`, '_blank');
			})
		}
		
		if(useOverlays){
			leftObj.style.transform = 'translateX(0px) translateY(' + elTrans + 'px) translateZ(0px) rotate(0deg)';
			leftObj.style.webkitTransform = 'translateX(0px) translateY(' + elTrans + 'px) translateZ(0px) rotate(0deg)';
			
			rightObj.style.transform = 'translateX(0px) translateY(' + elTrans + 'px) translateZ(0px) rotate(0deg)';
			rightObj.style.webkitTransform = 'translateX(0px) translateY(' + elTrans + 'px) translateZ(0px) rotate(0deg)';
			
			topObj.style.transform = 'translateX(0px) translateY(' + elTrans + 'px) translateZ(0px) rotate(0deg)';
			topObj.style.webkitTransform = 'translateX(0px) translateY(' + elTrans + 'px) translateZ(0px) rotate(0deg)';
			
		} else {
			leftObj.className = '';
			rightObj.className = '';
			topObj.className = '';
			
			leftObj.classList.add('stackedcards-overlay-hidden');
			rightObj.classList.add('stackedcards-overlay-hidden');
			topObj.classList.add('stackedcards-overlay-hidden');
		}
		
		//Remove class init
		setTimeout(function() {
			obj.classList.remove('init');
		},150);
		
		
		function backToMiddle() {
	
			removeNoTransition();
			transformUi(0, 0, 1, currentElementObj); 
	
			if(useOverlays){
				transformUi(0, 0, 0, leftObj);
				transformUi(0, 0, 0, rightObj);
				transformUi(0, 0, 0, topObj);
			}
	
			setZindex(15);
	
			if(!(currentPosition >= maxElements)){
				//roll back the opacity of second element
				if((currentPosition + 1) < maxElements){
					listElNodesObj[currentPosition + 1].style.opacity = '.8';
				}
			}
		};
		
		// Usable functions
		function countElements() {
			maxElements = listElNodesObj.length;
			if(items > maxElements){
				items = maxElements;
			}
		};
		
		//Keep the active card.
		function currentElement() {
		  currentElementObj = listElNodesObj[currentPosition];  
		};
		
		//Functions to swipe left elements on logic external action.
		function onActionLeft() {
			if(!(currentPosition >= maxElements)){
				if(useOverlays) {
					leftObj.classList.remove('no-transition');
					topObj.classList.remove('no-transition');
					leftObj.style.zIndex = '18';
					transformUi(0, 0, 1, leftObj);
	
				}
				
				setTimeout(function() {
					onSwipeLeft();
					resetOverlayLeft();
				},300);
			}
		};
		
		//Functions to swipe right elements on logic external action.
		function onActionRight() {
			if(!(currentPosition >= maxElements)){
				if(useOverlays) {
					rightObj.classList.remove('no-transition');
					topObj.classList.remove('no-transition');
					rightObj.style.zIndex = '18';
					transformUi(0, 0, 1, rightObj);
				}

				let id = listElNodesObj[currentPosition].children[0].id.slice(3)
				console.log(id)
				updateMyFavList(id)
					.then(resolve => {
						console.log(resolve)
						if (resolve == 201) {
							console.log("updated favList")
						} else {
							console.log("error in update favList")
						}
					})
					.catch(e => console.log(e))
	
				setTimeout(function(){
					onSwipeRight();
					resetOverlayRight();
				},300);
			}
		};
		
		//Functions to swipe top elements on logic external action.
		function onActionTop() {
			if(!(currentPosition >= maxElements)){
				if(useOverlays) {
					leftObj.classList.remove('no-transition');
					rightObj.classList.remove('no-transition');
					topObj.classList.remove('no-transition');
					topObj.style.zIndex = '18';
					transformUi(0, 0, 1, topObj);
				}
				
				setTimeout(function(){
					onSwipeTop();
					resetOverlays();
				},300); //wait animations end
			}
		};
		
		//Swipe active card to left.
		function onSwipeLeft() {
			removeNoTransition();
			transformUi(-1000, 0, 0, currentElementObj);
			if(useOverlays){
				transformUi(-1000, 0, 0, leftObj); //Move leftOverlay
				transformUi(-1000, 0, 0, topObj); //Move topOverlay
				resetOverlayLeft();
			}
			currentPosition = currentPosition + 1;
			updateUi();
			currentElement();
			setActiveHidden();
		};
		
		//Swipe active card to right.
		function onSwipeRight() {
			removeNoTransition();
			transformUi(1000, 0, 0, currentElementObj);
			if(useOverlays){
				transformUi(1000, 0, 0, rightObj); //Move rightOverlay
				transformUi(1000, 0, 0, topObj); //Move topOverlay
				resetOverlayRight();
			}
	
			currentPosition = currentPosition + 1;
			updateUi();
			currentElement();
			setActiveHidden();
		};
		
		//Swipe active card to top.
		function onSwipeTop() {
			removeNoTransition();
			transformUi(0, -1000, 0, currentElementObj);
			if(useOverlays){
				transformUi(0, -1000, 0, leftObj); //Move leftOverlay
				transformUi(0, -1000, 0, rightObj); //Move rightOverlay
				transformUi(0, -1000, 0, topObj); //Move topOverlay
				resetOverlays();
			}
	
			currentPosition = currentPosition + 1;
			updateUi();
			currentElement();
			setActiveHidden();
		};
		
		//Remove transitions from all elements to be moved in each swipe movement to improve perfomance of stacked cards.
		function removeNoTransition() {
			if(listElNodesObj[currentPosition]){
				
				if(useOverlays) {
					leftObj.classList.remove('no-transition');
					rightObj.classList.remove('no-transition');
					topObj.classList.remove('no-transition');
				}
				
				listElNodesObj[currentPosition].classList.remove('no-transition');
				listElNodesObj[currentPosition].style.zIndex = 16;
			}
			
		};
	
		//Move the overlay left to initial position.
		function resetOverlayLeft() {
			if(!(currentPosition >= maxElements)){
				if(useOverlays){
					setTimeout(function(){
						
						if(stackedOptions === "Top"){
							
							elTrans = elementsMargin * (items - 1);
						
						} else if(stackedOptions === "Bottom" || stackedOptions === "None"){
							
							elTrans = 0;
						
						}
						
						if(!isFirstTime){
							
							leftObj.classList.add('no-transition');
							topObj.classList.add('no-transition');
							
						}
						
						requestAnimationFrame(function(){
							
							leftObj.style.transform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
							leftObj.style.webkitTransform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
							leftObj.style.opacity = '0';
							
							topObj.style.transform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
							topObj.style.webkitTransform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
							topObj.style.opacity = '0';
						
						});
						
					},300);
					
					isFirstTime = false;
				}
			}
	   };
	   
		//Move the overlay right to initial position.
		function resetOverlayRight() {
			if(!(currentPosition >= maxElements)){
				if(useOverlays){
					setTimeout(function(){
						
						if(stackedOptions === "Top"){+2
							
							elTrans = elementsMargin * (items - 1);
						
						} else if(stackedOptions === "Bottom" || stackedOptions === "None"){
							
							elTrans = 0;
						
						}
						
						if(!isFirstTime){
							
							rightObj.classList.add('no-transition');
							topObj.classList.add('no-transition');
							
						}
						
						requestAnimationFrame(function(){
							
							rightObj.style.transform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
							rightObj.style.webkitTransform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
							rightObj.style.opacity = '0';
							
							topObj.style.transform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
							topObj.style.webkitTransform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
							topObj.style.opacity = '0';
						
						});
	
					},300);
					
					isFirstTime = false;
				}
			}
	   };
	   
		//Move the overlays to initial position.
		function resetOverlays() {
			if(!(currentPosition >= maxElements)){
				if(useOverlays){
	
					setTimeout(function(){
						if(stackedOptions === "Top"){
							
							elTrans = elementsMargin * (items - 1);
						
						} else if(stackedOptions === "Bottom" || stackedOptions === "None"){
	
							elTrans = 0;
	
						}
						
						if(!isFirstTime){
	
							leftObj.classList.add('no-transition');
							rightObj.classList.add('no-transition');
							topObj.classList.add('no-transition');
	
						}
						
						requestAnimationFrame(function(){
	
							leftObj.style.transform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
							leftObj.style.webkitTransform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
							leftObj.style.opacity = '0';
							
							rightObj.style.transform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
							rightObj.style.webkitTransform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
							rightObj.style.opacity = '0';
							
							topObj.style.transform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
							topObj.style.webkitTransform = "translateX(0) translateY(" + elTrans + "px) translateZ(0)";
							topObj.style.opacity = '0';
	
						});
	
					},300);	// wait for animations time
					
					isFirstTime = false;							
				}
			}
	   };
		
		function setActiveHidden() {
			if(!(currentPosition >= maxElements)){
				listElNodesObj[currentPosition - 1].classList.remove('stackedcards-active');
				listElNodesObj[currentPosition - 1].classList.add('stackedcards-hidden');
				listElNodesObj[currentPosition].classList.add('stackedcards-active');
			}		 
		};
	
		//Set the new z-index for specific card.
		function setZindex(zIndex) {
			if(listElNodesObj[currentPosition]){
				listElNodesObj[currentPosition].style.zIndex = zIndex;
			}		 
		};
	
    // Remove element from the DOM after swipe. To use this method you need to call this function in onSwipeLeft, onSwipeRight and onSwipeTop and put the method just above the variable 'currentPosition = currentPosition + 1'. 
    //On the actions onSwipeLeft, onSwipeRight and onSwipeTop you need to remove the currentPosition variable (currentPosition = currentPosition + 1) and the function setActiveHidden

		function removeElement() {
      currentElementObj.remove();
      if(!(currentPosition >= maxElements)){
				listElNodesObj[currentPosition].classList.add('stackedcards-active');
			}		
		};
		
		//Add translate X and Y to active card for each frame.
		function transformUi(moveX,moveY,opacity,elementObj) {
			requestAnimationFrame(function(){  
				let element = elementObj;
				
				// Function to generate rotate value 
				function RotateRegulator(value) {
				   if(value/10 > 15) {
					   return 15;
				   }
				   else if(value/10 < -15) {
					   return -15;
				   }
				   return value/10;
				}
				
				if(rotate){
					rotateElement = RotateRegulator(moveX);
				} else {
					rotateElement = 0;
				}
				
				if(stackedOptions === "Top"){
					elTrans = elementsMargin * (items - 1);
					if(element){     
						element.style.webkitTransform = "translateX(" + moveX + "px) translateY(" + (moveY + elTrans) + "px) translateZ(0) rotate(" + rotateElement + "deg)";
						element.style.transform = "translateX(" + moveX + "px) translateY(" + (moveY + elTrans) + "px) translateZ(0) rotate(" + rotateElement + "deg)";
						element.style.opacity = opacity;
					}
				} else if(stackedOptions === "Bottom" || stackedOptions === "None"){
					
					if(element){
						element.style.webkitTransform = "translateX(" + moveX + "px) translateY(" + (moveY) + "px) translateZ(0) rotate(" + rotateElement + "deg)";
						element.style.transform = "translateX(" + moveX + "px) translateY(" + (moveY) + "px) translateZ(0) rotate(" + rotateElement + "deg)";
						element.style.opacity = opacity;
					}
				
				}
			});	  
		};
	
		//Action to update all elements on the DOM for each stacked card.
		function updateUi() {
			requestAnimationFrame(function(){
				elTrans = 0;
				let elZindex = 15;
				let elScale = 1;
				let elOpac = 1;
				let elTransTop = items;
				let elTransInc = elementsMargin;
	
				for(i = currentPosition; i < (currentPosition + items); i++){
					if(listElNodesObj[i]){
						if(stackedOptions === "Top"){
	
							listElNodesObj[i].classList.add('stackedcards-top', 'stackedcards--animatable', 'stackedcards-origin-top');
	
							if(useOverlays){
								leftObj.classList.add('stackedcards-origin-top');
								rightObj.classList.add('stackedcards-origin-top');
								topObj.classList.add('stackedcards-origin-top'); 
							}
	
							elTrans = elTransInc * elTransTop;
							elTransTop--;
	
						} else if(stackedOptions === "Bottom"){
							listElNodesObj[i].classList.add('stackedcards-bottom', 'stackedcards--animatable', 'stackedcards-origin-bottom');
	
							if(useOverlays){
								leftObj.classList.add('stackedcards-origin-bottom');
								rightObj.classList.add('stackedcards-origin-bottom');
								topObj.classList.add('stackedcards-origin-bottom');
							}
	
							elTrans = elTrans + elTransInc;
	
						} else if (stackedOptions === "None"){
	
							listElNodesObj[i].classList.add('stackedcards-none', 'stackedcards--animatable');
							elTrans = elTrans + elTransInc;
	
						}
	
						listElNodesObj[i].style.transform ='scale(' + elScale + ') translateX(0) translateY(' + (elTrans - elTransInc) + 'px) translateZ(0)';
						listElNodesObj[i].style.webkitTransform ='scale(' + elScale + ') translateX(0) translateY(' + (elTrans - elTransInc) + 'px) translateZ(0)';
						listElNodesObj[i].style.opacity = elOpac;
						listElNodesObj[i].style.zIndex = elZindex;
	
						elScale = elScale - 0.04;
						elOpac = elOpac - (1 / items);
						elZindex--;
					}
				}
	
			});
		  
		};
	
		//Touch events block
		let element = obj;
		let startTime;
		let startX;
		let startY;
		let translateX;
		let translateY;
		let currentX;
		let currentY;
		let touchingElement = false;
		let timeTaken;
		let topOpacity;
		let rightOpacity;
		let leftOpacity;
	
		function setOverlayOpacity() {
	
			topOpacity = (((translateY + (elementHeight) / 2) / 100) * -1);
			rightOpacity = translateX / 100;
			leftOpacity = ((translateX / 100) * -1);
			
	
			if(topOpacity > 1) {
				topOpacity = 1;
			}
	
			if(rightOpacity > 1) {
				rightOpacity = 1;
			}
	
			if(leftOpacity > 1) {
				leftOpacity = 1;
			}
		}
		
		function gestureStart(evt) {
			console.log("start", evt.type)
			startTime = new Date().getTime();
			
			if (evt.type == "mousedown") {
				document.addEventListener('mouseup', gestureEnd, false);
				startX = evt.clientX;
				startY = evt.clientY;
			} else {
				startX = evt.changedTouches[0].clientX;
				startY = evt.changedTouches[0].clientY;
			}
			
			currentX = startX;
			currentY = startY;
	
			setOverlayOpacity();
			
			touchingElement = true;
			if(!(currentPosition >= maxElements)){
				if(listElNodesObj[currentPosition]){
					listElNodesObj[currentPosition].classList.add('no-transition');
					setZindex(16);
					
					if(useOverlays){
						leftObj.classList.add('no-transition');
						rightObj.classList.add('no-transition');
						topObj.classList.add('no-transition');
					}
					
					if((currentPosition + 1) < maxElements){
						listElNodesObj[currentPosition + 1].style.opacity = '1';
					}
					
					elementHeight = listElNodesObj[currentPosition].offsetHeight / 3;
				}
	
			}
			
		};
		
		function gestureMove(evt) {
			// console.log("move", evt)
			if(!touchingElement){
				return;
			}

			if (evt.type == "mousemove") {
				currentX = evt.pageX;
				currentY = evt.pageY;
			} else {
				currentX = evt.changedTouches[0].pageX;
				currentY = evt.changedTouches[0].pageY;
			}

			translateX = currentX - startX;
			translateY = currentY - startY;
	
			setOverlayOpacity();
			
			if(!(currentPosition >= maxElements)){
				evt.preventDefault();
				transformUi(translateX, translateY, 1, currentElementObj);
	
				if(useOverlays){
					// transformUi(translateX, translateY, topOpacity, topObj);
	
					if(translateX < 0){
						transformUi(translateX, translateY, leftOpacity, leftObj);
						transformUi(0, 0, 0, rightObj);
	
					} else if(translateX > 0){
						transformUi(translateX, translateY, rightOpacity, rightObj);
						transformUi(0, 0, 0, leftObj);
					}
	
					if(useOverlays){
						leftObj.style.zIndex = 18;
						rightObj.style.zIndex = 18;
						topObj.style.zIndex = -1;
					}
	
				}
	
			}
			
		};
		
		function gestureEnd(evt) {

			if(!touchingElement){
				return;
			}
			
			translateX = currentX - startX;
			translateY = currentY - startY;
			touchingElement = false;
			if (evt.type === 'mouseup') {
				document.removeEventListener('mouseup', gestureEnd, false);
				console.log("X",currentX, startX)
				console.log("Y", currentY, startY)
				console.log("trans", translateY, translateX)
				// if (translateY < 0 && (Math.abs(translateX) < 700)) {
				// 	console.log("up")
				// 	onSwipeTop()
				// 	return
				// }
			// 	if (translateX > listElNodesWidth / 2) {
			// 		console.log("right")
			// 		onSwipeRight()
			// 	} else {
			// 		console.log("left")
			// 		onSwipeLeft()
			// 	}
			// 	return
			}
			
			timeTaken = new Date().getTime() - startTime;
			

			
			if(!(currentPosition >= maxElements)){
				// if(translateY < (elementHeight * -1) && translateX > ((listElNodesWidth / 2) * -1) && translateX < (listElNodesWidth / 2)){  //is Top?
	
				// 	if(translateY < (elementHeight * -1) || (Math.abs(translateY) / timeTaken > velocity)){ // Did It Move To Top?
				// 		onSwipeTop();
				// 	} else {
				// 		backToMiddle();
				// 	}
	
				// } else {
	
					if(translateX < 0){
						if(translateX < ((listElNodesWidth / 2) * -1) || (Math.abs(translateX) / timeTaken > velocity)){ // Did It Move To Left?
							console.log("left")
							onSwipeLeft();
							listElNodesObj[currentPosition - 1].style.visibility = "hidden"
						} else {
							console.log("middle")
							backToMiddle();
						}
					} else if(translateX > 0) {
						
						if (translateX > (listElNodesWidth / 2) && (Math.abs(translateX) / timeTaken > velocity)){ // Did It Move To Right?
							console.log("right")

							let id = listElNodesObj[currentPosition].children[0].id.slice(3)
							console.log(id)
							updateMyFavList(id)
								.then(resolve => {
									console.log(resolve)
									if (resolve == 201) {
										console.log("updated favList")
									} else {
										console.log("error in update favList")
									}
								})
								.catch(e => console.log(e))
								
							onSwipeRight();
							listElNodesObj[currentPosition - 1].style.visibility = "hidden"						
						} else {
							console.log("middle")
							backToMiddle();
						}
	
					}
				// }
			}
		};

		element.addEventListener('touchstart', gestureStart, false);
		element.addEventListener('touchmove', gestureMove, false);
		element.addEventListener('touchend', gestureEnd, false);
		element.addEventListener('mousedown', gestureStart, false);
		element.addEventListener('mousemove', gestureMove, false);
		
		//Add listeners to call global action for swipe cards
		let buttonLeft = document.querySelector('.left-action');
		let buttonTop = document.querySelector('.top-action');
		let buttonRight = document.querySelector('.right-action');

		buttonLeft.addEventListener('click', onActionLeft, false);
		buttonTop.addEventListener('click', onActionTop, false);
		buttonRight.addEventListener('click', onActionRight, false);

	}
	
});

async function updateMyFavList(id) {
	try {
		let res = await fetch('/fav-list', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				keyId: id,
			}),
		})
		console.log("word",id)
		let message = await res.text()
		console.log(message)
		return res.status
	} catch (e) {
		console.log(e)
	}

}

// memosDiv.textContent = 'loading...'

// async function loadIdioms() {
//   let res = await fetch('/summary/api')
//   let memos = await res.json()
//   memosDiv.innerHTML = ''
//   for (let memo of memos) {
//     // console.log(memo)
//     memosDiv.innerHTML += `
//           <li class="memo" style="transform: rotate(${Math.floor(Math.random() * 10) - 5}deg)">
//             <article class="text">
//               <h3 class="h3-margin">${memo.key}</h3>
//               <p>${memo.meaning}</p>
//             </article>
//           </li>
//           `
//   }
//   let clips = document.querySelectorAll("li")
//   // console.log(clips)
//   clips.forEach((clip) => {
//     clip.classList.add("clips")
//     // clip.style.setProperty("--rotate", `rotate(${Math.floor(Math.random() * 15) - 7}deg)`)
//     // console.log(clip.style.getPropertyValue("--rotate"))
//   })
// }
// loadIdioms()

// addBtn.addEventListener("click", () => {
//   addForm.style.visibility = "visible"
//   let clips = document.querySelectorAll("li")
//   clips.forEach((clip) => clip.classList.toggle("clips"))

//   addForm.addEventListener("submit", submitHandle)
// })

// loginBtn.addEventListener("click", () => {
//   loginForm.style.visibility = "visible"

//   let clips = document.querySelectorAll("li")
//   clips.forEach((clip) => clip.classList.toggle("clips"))
// })

// async function deleteMemo(id) {
//   await fetch('/memos/' + id, {
//     method: 'DELETE',
//   })
//   loadMemos()
// }

// async function submitHandle(event) {
//   console.log("submit")
//   event.preventDefault()
//   // let formData = new FormData()
//   // formData.append('content',addForm.content.value)
//   // console.log("formdata",formData)
//   // console.log(addForm.content.value)
//   let res = await fetch('/memos', {
//     method: 'POST',
//     headers: {
//       'Content-Type': 'application/json',
//     },
//     body: JSON.stringify({
//       content: addForm.content.value,
//     }),
//   })
//   // let message = await res.text()
//   // console.log(message)
//   if (res.status === 201) {
//     console.log(res)
//     addForm.reset()
//     addForm.style.visibility = "hidden"
//     loadMemos()
//   } else {
//     alert(message)
//   }
// }

// logoutForm.addEventListener('submit', async event => {
//   event.preventDefault()
//   await fetch('/logout', { method: 'POST' })
//   checkRole()
// })

// manualLogin.addEventListener('submit', async event => {
//   event.preventDefault()
//   let res = await fetch('/login', {
//     method: 'POST',
//     headers: {
//       'Content-Type': 'application/json',
//     },
//     body: JSON.stringify({
//       username: manualLogin.username.value,
//       password: manualLogin.password.value,
//     }),
//   })
//   console.log('login res:', res)
//   if (res.status === 200) {
//     console.log("resstatus", res.status)
//     checkRole()
//   } else {
//     let message = await res.text()
//     Swal.fire({
//       icon: 'error',
//       title: 'Login Failed',
//       text: message,
//       footer: '<a href>Reset Password</a>',
//     })
//   }
// })

// googleLogin.addEventListener('click', () => {
//   window.location.href = '/connect/google'
// })

