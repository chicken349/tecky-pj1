// let toggleUpBtn = document.querySelector('.toggle-up')
// console.log("togg",toggleUpBtn)
let darkMode = true
let isLoggedIn = false
let sessionColorMode;


async function checkDarkMode(toggleUpBtn) {
	try {
		let userDetail = await checkUserDetail()

		if (userDetail) {
			darkMode = userDetail.dark_mode
			isLoggedIn = true
			console.log("check",darkMode)
		}

    if (userDetail?.profile_pic) {
      let loginBtn = document.querySelector("#login-a")
      let menu = document.querySelector(".menu")
  
      loginBtn.remove()
      let div = document.createElement('div')
      div.className = "button"
      div.id = "icon-container"
      let img = document.createElement('img')
      img.id = "user-icon"
      img.src = userDetail.profile_pic
      img.onclick = () => window.location = '/user/user-profile'
      let childDiv = document.createElement('div')
      childDiv.className = 'button-text-container'
      let p = document.createElement('p')
      p.id = 'login-text'
      p.textContent = 'Logout'
      childDiv.appendChild(p)
      div.appendChild(img)
      div.appendChild(childDiv)
      menu.appendChild(div)
  
      let logoutBtn = document.querySelector("#login-text")
  
      logoutBtn.addEventListener('click', () => {
        window.location = '/logout'
      })
    }

    if (userDetail?.favList) {
      await checkFavSession(userDetail)
    }

		await colorPreference()
		if (!isLoggedIn) {
			sessionColorMode = await (await fetch('/update-color-mode')).json()
			console.log("sessionColorMode",sessionColorMode)
			darkMode = sessionColorMode == "Light" ? false : true
			console.log("darkmodeafterCP",darkMode)
		}
	} catch (e) {
		console.log(e)
	}
  addToggleUpBtnEvent(toggleUpBtn)
}


function addToggleUpBtnEvent(toggleUpBtn) {
  initialSetup(toggleUpBtn)
  toggleUpBtn.addEventListener('click', () => toggleColor(toggleUpBtn))

}

function initialSetup(toggleUpBtn) {
  console.log("darkMode",darkMode)
  if (!darkMode) {
    darkMode = false
    document.body.style.backgroundColor = "#0E0707"
    document.documentElement.style.filter = "invert(1)"
    toggleUpBtn.innerHTML =`
      <i class="fas fa-moon fontAwesome-size dark-mode" id="up-dark-mode"></i>
      <div class="button-text-container">
        <p>Dark</p>
      </div>
    `
  } else {
    darkMode = true
    document.body.style.backgroundColor = "rgb(53, 54, 58)"
    document.documentElement.style.filter = ""
    toggleUpBtn.innerHTML =`
      <i class="fas fa-sun fontAwesome-size light-mode" id="up-light-mode"></i>
      <div class="button-text-container">
        <p>Light</p>
      </div>
    `
  }
}
function toggleColor(toggleUpBtn) {
  console.log("darkMode",darkMode)
  if (darkMode) {
    darkMode = false
    document.body.style.backgroundColor = "#0E0707"
    document.documentElement.style.filter = "invert(1)"
    toggleUpBtn.innerHTML =`
      <i class="fas fa-moon fontAwesome-size dark-mode" id="up-dark-mode"></i>
      <div class="button-text-container">
        <p>Dark</p>
      </div>
    `
  } else {
    darkMode = true
    document.body.style.backgroundColor = "rgb(53, 54, 58)"
    document.documentElement.style.filter = ""
    toggleUpBtn.innerHTML =`
      <i class="fas fa-sun fontAwesome-size light-mode" id="up-light-mode"></i>
      <div class="button-text-container">
        <p>Light</p>
      </div>
    `
  }
}
