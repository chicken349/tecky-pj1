const chatForm = document.getElementById('chat-form');
const chatMessages = document.querySelector('.chat-messages');
const roomName = document.getElementById('room-name');
const roomNameOverlay = document.getElementById('room-name-2');
const searchInput = document.querySelector(".search-input")
const responseList = document.querySelector('.search-response-list')
const userList = document.getElementById('users');
const userListOverlay = document.getElementById('users-2');
let newMessageSent = false


runChatroom()

// Get username and room from URL
async function runChatroom() {
  const { username, room } = Qs.parse(location.search, {
    ignoreQueryPrefix: true,
  });
  console.log("first",username)
  const searchForm = document.querySelector(".search-form")

  
  const socket = io();
  
  // Join chatroom
  socket.emit('joinRoom', { username, room });
  
  // Get room and users
  socket.on('roomUsers', ({ room, users }) => {
    outputRoomName(room);
    outputUsers(users);
    loadHistory().then().catch(e => console.log(e))
  });
  
  
  async function loadHistory() {
    let res = await fetch(`/chatroom-history?roomName=${roomName.innerText}`)
    let messageHistory = await res.json()
  
    for (let message of messageHistory) {
      outputMessage(message.message);
    }
    chatMessages.lastElementChild.scrollIntoView({behavior: 'smooth'})
    console.log("usernameload",username)
  }
  
  // Message from server
  socket.on('message', (message) => {
    console.log("usernamemessage",username)
    console.log("output", message);
    outputMessage(message);
    // console.log("newMessageSent",message.username)
    if (newMessageSent && message.username === username) {
      submitHandle(message)
      newMessageSent = false
    }

    // Scroll down
    chatMessages.lastElementChild.scrollIntoView({behavior: 'smooth'})
  });
  
  // Message submit
  chatForm.addEventListener('submit', (e) => {
    e.preventDefault();
    console.log(e.target.chatInput.value)
    let msgInput = document.querySelector("#new-message")
    // Get message text
    let msg = msgInput.value
    console.log(msg)
    // let msg = e.target.elements.msg.innerText;
  
    msg = msg.trim();
  
    if (!msg) {
      return false;
    }
  
    // Emit message to server
    socket.emit('chatMessage', msg);
    console.log("chatroomName",roomName)
    // Clear input
    list.innerHTML = ''
    preview.innerHTML = ''
    msgInput.value = '';
    let meaningEl = document.querySelector('.suggestion-word-meaning')
    if (meaningEl) {
      meaningEl.remove()
    }
    msgInput.focus();
  
    newMessageSent = true
  });
  
  async function submitHandle(msg) {
    console.log("submitHandle")
    let res = await fetch('/chatroom-history', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        msg: msg,
        chatroomName: roomName.innerText,
      }),
    })
    let message = await res.text()
    console.log("message", message)
    if (!res.status === 201) {
      // console.log(res)
    // } else {
      alert(message)
    }
  }
  
  // Output message to DOM
  function outputMessage(message) {
    const div = document.createElement('div');
    div.classList.add('message');
    const p = document.createElement('p');
    p.classList.add('meta');
    p.innerText = message.username;
    if (message.username == username) {
      div.style.direction = "rtl"
      div.style.backgroundColor = "rgb(220, 248, 198)"
      p.innerText = ''
    }
    p.innerHTML += `<span>${message.time}</span>`;
    div.appendChild(p);
    const para = document.createElement('p');
    para.classList.add('text');
    para.innerText = message.text;
    div.appendChild(para);
    document.querySelector('.chat-messages').appendChild(div);
  }
  
  // Add room name to DOM
  function outputRoomName(room) {
    roomName.innerText = room;
    roomNameOverlay.innerText = room;
  }
  
  // Add users to DOM
  function outputUsers(users) {
    userList.innerHTML = '';
    userListOverlay.innerHTML = '';
    users.forEach((user) => {
      const li = document.createElement('li');
      li.innerText = user.username;
      userList.appendChild(li);
      const liOverlay = document.createElement('li');
      liOverlay.innerText = user.username;
      userListOverlay.appendChild(liOverlay);
    });
  }
  
  //Prompt the user before leave chat room
  document.getElementById('leave-btn').addEventListener('click', () => {
    const leaveRoom = confirm('Are you sure you want to leave the chatroom?');
    // console.log("leaveRoom", leaveRoom)
    if (leaveRoom) {
      window.location = '/chat';
    }
  });
  let overlayFirstLayer = document.querySelector('.overlay-first-layer')
  let overlaySecondLayer = document.querySelector('.overlay-second-layer')
  document.getElementById('members').addEventListener('click', () => {
    overlay.classList.add('open')
		overlay.classList.remove('initial-state')
    overlayFirstLayer.style.visibility = "hidden"
    overlaySecondLayer.style.visibility = "visible"
  });

  // open
  searchForm.addEventListener('submit', async (e) => {
    e.preventDefault()
    overlayFirstLayer.style.visibility = "visible"
    overlaySecondLayer.style.visibility = "hidden"
    overlay.classList.add('open')
		overlay.classList.remove('initial-state')
    let keyEl = document.querySelector('#idioms .key')
    let meaningsUl = document.querySelector('#idioms .meanings')
    let situationsUl = document.querySelector('#idioms .idioms-situation')
    let examplesUl = document.querySelector('#idioms .examples')

    let searchResult = await (await fetch(`/search-word?key=${searchForm.search.value}`)).json()
    let id = searchResult.id
    let res = await fetch(`/search/${id}`)
    let resFormatted = await res.json()
    let details = resFormatted.idiomDetails
    let idiomTypes = resFormatted.idiomTypes
    console.log(details)
    
    searchInput.value = ''
    keyEl.innerHTML = ''
    meaningsUl.innerHTML = '<p>意思:</p>'
    situationsUl.innerHTML = '<p>分類:</p>'
    examplesUl.innerHTML = ''

    keyEl.id = 'key' + id
    keyEl.innerHTML = `<h3>${details.meanings[0].key}</h3>`

    // console.log(details[3])
    for (let meaning of details.meanings) {
      meaningsUl.innerHTML += `
        <li class="box-shadow-list">${meaning.meaning}</li>
      `
    }

    for (let situation of details.idiom_situations) {
      situationsUl.innerHTML += `
        <li class="inline-list">${situation.type}</li>
      `
    }

    console.log(details.examples)
    for (let i = 0; i < details.examples.length; i++) {
      examplesUl.innerHTML += `
        <li class="box-shadow-list">例子${i + 1}: ${details.examples[i].example}</li>
      `
    }
  })

  // close
  document.querySelector('#overlay-background').addEventListener('click',restoreState);
  document.querySelector('#overlay-close').addEventListener('click',restoreState);
  document.querySelector('#overlay-close-2').addEventListener('click',restoreState);

  function restoreState() {
    overlay.classList.remove('open')
  }

  searchForm.addEventListener('mouseenter', () => {
    if (isMobile) {
      let bodyWidth = document.body.getBoundingClientRect().width
      searchForm.style.width = Math.min((bodyWidth - 50 - 10 - 15 - 15 - 50 - 30), 300).toString() + "px"
    } else {
      searchForm.style.width = "300px"
    }
    searchInput.style.display = "block"
  })
  let msgForm = document.querySelector('.chat-form-and-preview-container')

  addClickEventListenerRemoveSearchListAndClose(chatMessagesBox, searchInput, responseList, searchForm)
  addClickEventListenerRemoveSearchListAndClose(chatSideBar, searchInput, responseList, searchForm)
  addClickEventListenerRemoveSearchListAndClose(msgForm, searchInput, responseList, searchForm)

  searchInput.addEventListener('input', searchPreview)

  async function searchPreview(e) {
    let chatMessagesBox = document.querySelector('.chat-messages')
    let chatSideBar = document.querySelector('.chat-sidebar')
    let searchImg = document.querySelector('.search-position')
    
    responseList.addEventListener('click', () => {
      responseList.innerHTML = ''
      responseList.style.visibility = "hidden"
      searchForm.dispatchEvent(new Event('submit'))
    })

    searchImg.addEventListener('click', () => {
      responseList.innerHTML = ''
      responseList.style.visibility = "hidden"
      if (searchInput.value) {
        searchForm.dispatchEvent(new Event('submit'))
      }
    })

    scheduleLoadListToSearch(searchInput, responseList, 'search-word')
  }
}

