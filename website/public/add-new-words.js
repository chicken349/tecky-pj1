// const content = document.querySelector('#overlay-content');


console.log(window.innerHeight)

const calculateHeight = function() {
	
	const content = document.querySelector('#overlay-content');
	const contentHeight = parseInt(content.offsetHeight) + parseInt(window.getComputedStyle(content).marginTop) + parseInt(window.getComputedStyle(content).marginBottom);
	const windowHeight = window.innerHeight;
	
	const finalHeight = windowHeight > contentHeight ? windowHeight : contentHeight;
	
	return finalHeight + "px";
};

let overlay = document.querySelector('#overlay')
let selectEl = document.querySelector('#register-type')

document.addEventListener("DOMContentLoaded", function() {

	updateSelectOptions()
	
	window.addEventListener("resize", function() {
    console.log("resize")

		if ((window.innerHeight < 560) && (window.innerWidth > 600)) {
			overlay.classList.add('short');
		} else {
			overlay.classList.remove('short');
		}
		
		return document.querySelector('#overlay-background').style.height = calculateHeight();
	});
	
	window.dispatchEvent(new Event('resize'));
	
	// open
	document.querySelector('#add').addEventListener('click',() => {
    overlay.classList.add('open')
		overlay.classList.remove('initial-state')
    overlay.querySelector('.register-form input:first-of-type').select()
	});
	
	// close
  document.querySelector('#overlay-background').addEventListener('click',restoreState);
  document.querySelector('#overlay-close').addEventListener('click',restoreState);
  document.querySelector('#overlay-close-2').addEventListener('click',restoreState);

	function restoreState() {
		overlay.classList.remove('open')
	}
});

async function updateSelectOptions() {
	let res = await fetch('/select-options')
  let selectOptions = await res.json()
	console.log(selectOptions)

	selectOptions.selectOptions.forEach((selectOption) => {
		selectEl.innerHTML += `<option value=${selectOption.type}>${selectOption.type}</option>`
	})

	selectEl.innerHTML += `<option value="其他">其他...</option>`
}

let message = document.querySelector(".message")
let formGive = document.querySelector(".form-give")
let formEdit = document.querySelector(".form-edit")
let overlayFirstLayer = document.querySelector(".overlay-first-layer")
let overlaySecondLayer = document.querySelector(".overlay-second-layer")
let submitNewWordBtn = document.querySelector("#submit-new-word-click")
let submitNewWordForm = document.querySelector("#submit-new-word-form")

// document.querySelector("#edit-old-word").addEventListener('click',function() {
//   message.style.transform =  "translateX(100%)";
//   message.style.borderLeft = "solid 1px blue"
//   message.style.borderRight = "none"
//   formGive.style.marginRight = "1px"
//   formEdit.style.marginLeft = "1px"
// 	overlayFirstLayer.style.visibility = "visible"
// 	overlaySecondLayer.style.visibility = "hidden"
// });

// document.querySelector("#give-new-word").addEventListener('click',function() {
//   message.style.transform = "translateX(0)";
//   message.style.borderLeft = "none"
//   message.style.borderRight = "solid 1px red"
//   formEdit.style.marginLeft = "1px"
//   formGive.style.marginRight = "1px"
// 	overlayFirstLayer.style.visibility = "hidden"
// 	overlaySecondLayer.style.visibility = "visible"
// });

submitNewWordForm.addEventListener('submit', submitHandle)

async function submitHandle(event) {
	event.preventDefault()

	let res = await fetch('/new-word', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({
			key: submitNewWordForm.key.value,
			type: submitNewWordForm.type.value,
			example: submitNewWordForm.example1.value,
			meaning: submitNewWordForm.meaning.value,
		}),
	})
	let message = await res.text()
	console.log(message)
	if (res.status === 201) {
		console.log(res)
		submitNewWordForm.reset()
		overlay.classList.remove('open')
	} else {
		alert(message)
	}
}






let addBtn = document.querySelector('#button-container')

function toggleAddBtnZIndex() {


	addBtn.addEventListener('mouseenter', (e) => {
		console.log("mouseenter")
		e.target.style.zIndex = "1000"

		setTimeout(() => e.target.style.zIndex = "4", 300)
	})

	// addBtn.addEventListener('mouseleave', (e) => {
	// 	console.log("mouseleave")
	// 	e.target.style.zIndex = "4"
	// })

	// addBtn.addEventListener('mousedown', (e) => {
	// 	console.log("mousedown")
	// 	e.target.style.zIndex = "1000"
	// })

	// addBtn.addEventListener('mouseup', (e) => {
	// 	console.log("mouseup")
	// 	e.target.style.zIndex = "4"
	// })

	addBtn.addEventListener('touchstart', (e) => {
		console.log("touchstart")
		e.target.style.zIndex = "1000"

		setTimeout(() => e.target.style.zIndex = "4", 300)
	})

	// addBtn.addEventListener('touchend', (e) => {
	// 	console.log("touchend")
	// 	e.target.style.zIndex = "1000"
	// })
}
