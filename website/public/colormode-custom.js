let darkMode = true
let toggleUpBtn = document.querySelector('.toggle-up')
let darkBg = ["rgb(53, 54, 58)", "rgb(29, 45, 80)", "#486581"]
let lightBg = ["#f4f9f9", "#ccf2f4", "rgb(253, 230, 234)"]
let isLoggedIn = false
let sessionColorMode;


async function checkDarkMode() {
	try {
		let userDetail = await checkUserDetail()

		if (userDetail) {
			darkMode = userDetail.dark_mode
			isLoggedIn = true
			console.log("check",darkMode)
		}

		if (userDetail?.profile_pic) {
			let loginBtn = document.querySelector("#login-a")
			let menu = document.querySelector(".menu")
	
			loginBtn.remove()
			let div = document.createElement('div')
			div.className = "button"
			div.id = "icon-container"
			let img = document.createElement('img')
			img.id = "user-icon"
			img.src = userDetail.profile_pic
			img.onclick = () => window.location = '/user/user-profile'
			let childDiv = document.createElement('div')
			childDiv.className = 'button-text-container'
			let p = document.createElement('p')
			p.id = 'login-text'
			p.textContent = 'Logout'
			childDiv.appendChild(p)
			div.appendChild(img)
			div.appendChild(childDiv)
			menu.appendChild(div)
	
			let logoutBtn = document.querySelector("#login-text")
	
			logoutBtn.addEventListener('click', () => {
				window.location = '/logout'
			})
		}
	
			// let colorBtn = document.querySelector('.toggle-up')
			// checkDarkMode(colorBtn, userDetail)

		await colorPreference()
		if (!isLoggedIn) {
			sessionColorMode = await (await fetch('/update-color-mode')).json()
			console.log("sessionColorMode",sessionColorMode)
			darkMode = sessionColorMode == "Light" ? false : true
			console.log("darkmodeafterCP",darkMode)
		}
	} catch (e) {
		console.log(e)
	}
  toggleContainerColor()
}

function toggleContainerColor() {
	
	// tricky way to use same function at initial state
	darkMode = !darkMode
	toggleColor()

	toggleUpBtn.addEventListener('click', toggleColor)
}

function toggleColor() {
	let headerBtns = document.querySelectorAll('.header .menu *')
  let addBtn = document.querySelector('#add')
	let sections = document.querySelectorAll('section')
	let containers = document.querySelectorAll('.container-animation')
  let data_tables = document.querySelectorAll('.data_table')

	if (darkMode) {
		darkMode = false
		document.body.style.backgroundColor = "#f4f9f9"
		for (let i = 0; i < sections.length; i++) {
			sections[i].style.backgroundColor = lightBg[i]
			containers[i].style.backgroundColor = lightBg[containers[i].id]
			containers[i].style.color = "black"
		}
		for (let headerBtn of headerBtns) {
			headerBtn.style.color = "black"
		}
		for (let data_table of data_tables) {
			data_table.style.setProperty("--scroll-bar-color-hover", "rgba(53, 54, 58, 0.7)")
		}
		toggleUpBtn.innerHTML =`
			<i class="fas fa-moon fontAwesome-size dark-mode" id="up-dark-mode"></i>
			<div class="button-text-container">
				<p>Dark</p>
			</div>
		`
	} else {
		darkMode = true
		document.body.style.backgroundColor = "rgb(53, 54, 58)"
		for (let i = 0; i < sections.length; i++) {
			sections[i].style.backgroundColor = darkBg[i]
			containers[i].style.backgroundColor = darkBg[containers[i].id]
			containers[i].style.color = "white"
		}
		for (let headerBtn of headerBtns) {
			headerBtn.style.color = "white"
		}
		for (let data_table of data_tables) {
			data_table.style.setProperty("--scroll-bar-color-hover", "rgba(	227, 227, 227, 0.88)")
		}
		toggleUpBtn.innerHTML =`
			<i class="fas fa-sun fontAwesome-size light-mode" id="up-light-mode"></i>
			<div class="button-text-container">
				<p>Light</p>
			</div>
		`
	}
}