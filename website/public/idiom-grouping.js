function updateResultTable(idiomResults) {
  for (let idiomResult of idiomResults) {
    result_table.innerHTML += `
      <li class="data_block">
        <a href='/search?id=${idiomResult.id}'>
          <div class="data_block_container">
            <div class="data_details">
              ${idiomResult.key}
            </div>
          </div>
        </a>
      </li>
    `
  }
}

function updateTypeTable(idiomTypes) {
  for (let idiomType of idiomTypes) {
    type_table.innerHTML += `
      <li class="data_block">
        <a href='/group?id=${idiomType.id}&input=${idiomType.type}'>
          <div class="data_block_container">
            <div class="data_details">
              ${idiomType.type}
            </div>
          </div>
        </a>
      </li>
    `
  }
}

function updateYearTable(thisYear) {
  for (let i = 0; i < thisYear - 2009; i++) {
    year_table.innerHTML += `
      <li class="data_block">
        <a href='/group?input=${thisYear - i}'>
          <div class="data_block_container">
            <div class="data_details">${thisYear - i}</div>
          </div>
        </a>
      </li>
    `
  }
}