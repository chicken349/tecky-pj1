async function colorPreference() {
    let colorPref = document.querySelector(".toggle-up")
    
    colorPref.addEventListener('click', submitHandle)

    async function submitHandle(event) {
        event.preventDefault()
        let colorPreferences = colorPref.querySelector("p").textContent
        console.log(colorPreferences)

        let userDetail = await checkUserDetail()
        console.log(userDetail)

        if (userDetail?.email) {

            let res = await fetch(`/colorPref?color=${colorPreferences}`)

            if (res.status === 201) {
                console.log('updated')
            } else if (res.status === 500) {
                console.log('failed')
            }
            colorPref.removeEventListener('click', submitHandle)

            colorPref = document.querySelector(".toggle-up")

            colorPref.addEventListener('click', submitHandle)
            return
        }   

		fetch(`/update-color-mode?color=${colorPreferences}`)
			.then().catch(e => console.log(e))
    }
}



