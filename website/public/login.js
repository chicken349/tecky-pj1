async function checkRole() {
  let res = await fetch('/role')
  let role = await res.text()
  if (role === 'admin') {
		console.log('admin')
    // document.querySelector('#login-text').textContent = "Logout"
    // document.querySelector('#login-a').href = "/logout"
    return 'admin'
  }
	if (role === 'member') {
    console.log('member')
    // document.querySelector('#login-text').textContent = "Logout"
    // document.querySelector('#login-a').href = "/logout"
    return 'member'
  }
  if (role === 'guest') {
    console.log('guest')
    document.querySelector('#login-text').textContent = "Login"
    document.querySelector('#login-a').href = "/connect/google"
    return 'guest'
  }
}
checkRole()