let sections = document.querySelectorAll('section')

function animateFirstTime() {
  
  sections[0].classList.remove("temporary")
  sections[0].classList.add("animate-first-time")
    
  setTimeout(() => {
    sections[0].classList.remove("animate-first-time")
  }, 600); 
}

function presentDataAnimation(i) {
  sections[i].classList.add("animate-first-time")
    
  setTimeout(() => {
    sections[i].classList.remove("animate-first-time")
  }, 600); 
}

function toggleSection(types) {
  let sectionTypes = types
  let containerDetails = [{type: types[0], backgroundColor: darkMode ? darkBg[0] : lightBg[0]}, {type: types[1], backgroundColor: darkMode ? darkBg[1] : lightBg[1]}, {type: types[2], backgroundColor: darkMode ? darkBg[2] : lightBg[2]}]
  let containers = document.querySelectorAll('.container-animation')
  let header = document.querySelector('header')
  let body = document.body

  for (let j = 0; j < containers.length; j++) {
    containers[j].addEventListener('click', toggleBg)
    
    function toggleBg() {
      let type = containerDetails[j].type
      let matchIndex;
      let newContainerIndex;

      for (let i = 0; i < sections.length; i++) {
        if (j == 0) {
          continue;
        }

        console.log('type',type)
        console.log('section',sectionTypes[i])
        if (type != sectionTypes[i]) {
          sections[i].classList.add("hidden")
        } else {
          newContainerIndex = sectionTypes.indexOf(containerDetails[0].type)
          console.log('newContainerIndex',newContainerIndex)

          matchIndex = j
          sections[i].classList.remove("hidden")
          sections[i].classList.remove("temporary")
          containerDetails[0].backgroundColor = darkMode ? darkBg[newContainerIndex] : lightBg[newContainerIndex]
          containerDetails[j].backgroundColor = darkMode ? darkBg[i] : lightBg[i]
          console.log("darkmode",darkMode)
          console.log('container',containerDetails[j].backgroundColor)
          
          containers[0].id = i
          body.style.backgroundColor = containerDetails[j].backgroundColor
          // sections[i].style.backgroundColor = containerDetails[j].backgroundColor


          containers[0].children[0].children[0].innerHTML = `
            ${containerDetails[j].type}
          `
          containers[j].children[0].children[0].innerHTML = `
            ${containerDetails[0].type} <i class="fas fa-arrows-alt-h arrow-rotate"></i>
          `

          presentDataAnimation(i)
        }
      }
      console.log(matchIndex)
      containerDetails.splice(matchIndex,0,containerDetails.splice(0,1)[0])
      containerDetails.splice(0,0,containerDetails.splice(matchIndex-1,1)[0])
      console.log(containerDetails)

      containers[0].style.backgroundColor = 'transparent'
      containers[j].id = newContainerIndex

      containers[j].style.backgroundColor = containerDetails[j].backgroundColor
    }
  }
}
