import express, {Request, Response} from 'express';
import path from 'path';
import { userRoute } from './user-route'
import { profileRoute } from './user-profile'
import { idiomSuggestRoute } from './idiom-suggestion'
import expressSession from 'express-session'
import {Server as SocketIO} from 'socket.io';
import { isLoggedIn } from './guards'
import http from 'http'; 
import grant from 'grant'
import formatMessage from './utils/messages';
import { db } from './db'
import {
  userJoin,
  getCurrentUser,
  userLeave,
  getRoomUsers
} from './utils/users';


const app = express()
const server = http.createServer(app);
const io = new SocketIO(server);

app.use(express.urlencoded({ extended: true }));
app.use(express.json())
app.use(express.static('./public'))

let sessionMiddleware = expressSession({
  secret: process.env.SESSION_SECRET as string,
  saveUninitialized: true,
  resave: true,
})
app.use(sessionMiddleware)

// http://localhost:8080
// https://teckyproject1chiuyu.loca.lt

const grantExpress = grant.express({
  defaults: {
    origin: `http://localhost:8080`,
    transport: 'session',
    state: true,
  },
  google: {
    key: process.env.GOOGLE_CLIENT_ID || '',
    secret: process.env.GOOGLE_CLIENT_SECRET || '',
    scope: ['profile', 'email'],
    callback: '/login/google',
  },
})

app.use(grantExpress as express.RequestHandler)

app.get('/join_chatroom', (req,res)=>{
  console.log(req.query)
  res.sendFile(path.join(__dirname, 'public','join_chatroom.html'))

})

app.get('/chatroom', (req,res)=>{
  console.log(req.query)
  res.sendFile(path.join(__dirname, 'public','chatmain.html'))
})

app.post('/search', (req,res)=>{
  console.log(req.query)
  res.sendFile(path.join(__dirname, 'public','search.html'))

})

app.post('/summary', (req,res)=>{
  console.log(req.query)
  res.sendFile(path.join(__dirname, 'public','summary.html'))
})






app.use(profileRoute)


// Set static folder
// app.use(express.static(path.join(__dirname, 'public')));
app.use('/user', isLoggedIn, express.static('protected'))
app.use(userRoute)
app.use(idiomSuggestRoute)


const botName = '潮語聊天室';

// Run when client connects
io.on('connection', socket => {
  socket.on('joinRoom', ({ username, room }) => {
    const user = userJoin(socket.id, username, room);

    socket.join(user.room);

    // Welcome current user
    socket.emit('message', formatMessage(botName, `Welcome, ${user.username}!` ));

    // Broadcast when a user connects
    socket.broadcast
      .to(user.room)
      .emit(
        'message',
        formatMessage(botName, `${user.username} has joined the chat`)
      );

    // Send users and room info
    io.to(user.room).emit('roomUsers', {
      room: user.room,
      users: getRoomUsers(user.room)
    });
  });

  // Listen for chatMessage
  socket.on('chatMessage', msg => {
    const user = getCurrentUser(socket.id);
    console.log("test",socket.id, user)

    let message = formatMessage(user?.username, msg)
    console.log("new message",message, msg?.room)

    io.to(user.room).emit('message', message);

    
    // insertMsg(message, msg.chatroomName)
    //   .then(() => console.log('inserted msg', message, msg.chatroom))
    //   .catch((e) => console.log)
  });

  // Runs when client disconnects
  socket.on('disconnect', () => {
    const user = userLeave(socket.id);

    if (user) {
      io.to(user.room).emit(
        'message',
        formatMessage(botName, `${user.username} has left the chat`)
      );

      // Send users and room info
      io.to(user.room).emit('roomUsers', {
        room: user.room,
        users: getRoomUsers(user.room)
      });
    }
  });
});

app.post('/chatroom-history', insertMsg)

async function insertMsg(req: Request, res: Response) {

  let msg: any = req.body?.msg
  let chatroomName: string = req.body?.chatroomName
  console.log("insertMsg", msg, chatroomName)

  try {
    await db.query(/* sql */ `
    INSERT INTO chatroom_data (message, room) VALUES ($1, $2)
  `,[msg, chatroomName])
    console.log("inserted message")
    res.status(201).end("inserted message")
  } catch (e) {
    console.log('Failed to insert history' + msg.text + chatroomName)
    res.status(500).end('Failed to insert history' + msg.text + chatroomName)
  }

}

app.get('/chatroom-history', getChatHistory)

async function getChatHistory(req: Request, res: Response) {
  let roomName = req.query.roomName
  console.log("roomName",roomName)
  try {
    let messageHistory = (await db.query(/* sql */`SELECT message from chatroom_data where room = $1`,[roomName])).rows
    console.log("messageHistory",messageHistory)
    res.json(messageHistory)
  } catch (e) {
    console.log(e)
    res.status(500).end('Failed to load history')
  }
}



app.use((req, res) => {
  res.sendFile(path.join(__dirname, 'public', '404.html'))
})


let PORT = 8080;

server.listen(PORT, () => {
    console.log(`listening on http://localhost:${PORT}`);
  });

