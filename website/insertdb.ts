// be careful not to insert 2 times, can comment out unneeded queries!!!!
// Not use in production!!

import { Client } from "pg";
import XLSX from "xlsx";
import dotenv from "dotenv";

dotenv.config();

export const db = new Client({
  database: process.env.DB_NAME,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
});

db.connect()
  .then(() => console.log("connected now"))
  .catch((err) => console.log(err));


  
let workbook = XLSX.readFile("../db/database-re.xlsx");

// add users

function queryUsers() {
  let usersSheet = workbook.Sheets["users"];
  let users: any[] = XLSX.utils.sheet_to_json(usersSheet);
  for (let user of users) {
    db.query(
      "insert into users (username, email, is_admin, is_banned, created_at, updated_at, dark_mode) values ($1, $2, $3, $4, NOW(), NOW(), true)",
      [user.username ,user.email, user.is_admin, user.is_banned]
    )
      .then(() => {
        console.log("added user");
      })
      .catch((e) => {
        console.log(e);
      });
  }
}
queryUsers();

// add idiom_types

function queryIdiomTypes() {
  let idiom_typesSheet = workbook.Sheets["idiom_types"];
  let idiom_types: any[] = XLSX.utils.sheet_to_json(idiom_typesSheet);
  for (let type of idiom_types) {
    let queryData = [type.type];
    db.query("insert into idiom_types (type) values ($1)", queryData)
      .then(() => {
        console.log(`added ${queryData}`);
      })
      .catch((e) => {
        console.log(e);
      });
  }
}
queryIdiomTypes();

// add idiom_types

function querySituationTypes() {
  let situation_typesSheet = workbook.Sheets["situation_types"];
  let situation_types: any[] = XLSX.utils.sheet_to_json(situation_typesSheet);
  for (let type of situation_types) {
    let queryData = [type.type];
    db.query("insert into situation_types (type) values ($1)", queryData)
      .then(() => {
        console.log(`added ${queryData}`);
      })
      .catch((e) => {
        console.log(e);
      });
  }
}
querySituationTypes();

// add idioms (main dish)

function queryIdioms() {
  let idiomsSheet = workbook.Sheets["idioms"];
  let idioms: any[] = XLSX.utils.sheet_to_json(idiomsSheet);
  let dataType = [
    "key",
    "year_popular",
    "idiom_type",
    "created_at",
    "updated_at",
  ];
  let dataTypeStr = dataType.reduce((acc, curr) => {
    acc += curr + ",";
    return acc;
  }, "");
  let queryData: any[] = [];
  let queryDataStr = "";

  for (let i = 1; i <= idioms.length * dataType.length; i++) {
    if (i % dataType.length == 0) {
      queryDataStr += "$" + i + "), ";
    } else if (i % dataType.length == 1) {
      queryDataStr += "($" + i + ",";
    } else {
      queryDataStr += "$" + i + ",";
    }

    if (i <= idioms.length) {
      queryData.push(idioms[i - 1].key);
      queryData.push(idioms[i - 1].year_popular);
      queryData.push(idioms[i - 1].idiom_type);
      queryData.push("NOW()");
      queryData.push("NOW()");
    }
  }
  let queryStr = `insert into idioms (${dataTypeStr.slice(
    0,
    dataTypeStr.length - 1
  )}) values ${queryDataStr.slice(0, queryDataStr.length - 2)}`;

  // console.log("dataTypeStr",dataTypeStr)
  // console.log("queryDataStr",queryDataStr)
  // console.log("queryData",queryData)
  db.query(queryStr, queryData)
    .then(() => {
      console.log(`added ${queryData}`);
    })
    .catch((e) => {
      throw Error(e);
    });
}
queryIdioms();

// // add meanings

async function queryMeanings() {
  let meaningsSheet = workbook.Sheets['meanings']
  let meanings: any[] = XLSX.utils.sheet_to_json(meaningsSheet)
  for (let meaning of meanings) {
    try {
      let idiom_id = (await db.query(`SELECT id from idioms where key = $1`,[meaning.key])).rows[0].id
      // console.log(idiom_id)
      let user_id = (await db.query(`SELECT id from users where username = $1`,[meaning.username])).rows[0].id
      await db.query(
        'insert into meanings (idiom_id, user_id, meaning) values ($1, $2, $3)',
        [idiom_id, user_id, meaning.meaning])
    } catch(e) {
      console.log(e)
    }
  }
}
queryMeanings()

// add situation_types

async function queryIdiomSituations() {
  let idiom_situationsSheet = workbook.Sheets['idiom_situations']
  let idiom_situations: any[] = XLSX.utils.sheet_to_json(idiom_situationsSheet)
  for (let idiom_situation of idiom_situations) {
    try {
      let idiom_id = (await db.query(`SELECT id from idioms where key = $1`,[idiom_situation.key])).rows[0].id
      // console.log(idiom_id)
      let situation_types_id = (await db.query(`SELECT id from situation_types where type = $1`,[idiom_situation.type])).rows[0].id

      await db.query(
        'insert into idiom_situations (idiom_id, situation_types_id) values ($1, $2)',
        [idiom_id, situation_types_id])
    } catch(e) {
      console.log(e)
    }
  }
}
queryIdiomSituations()

// add examples

async function queryExamples() {
  let examplesSheet = workbook.Sheets['examples']
  let examples: any[] = XLSX.utils.sheet_to_json(examplesSheet)
  for (let example of examples) {
    try {
      let idiom_id = (await db.query(`SELECT id from idioms where key = $1`,[example.key])).rows[0].id
      // console.log(idiom_id)
      await db.query(
        'insert into examples (idiom_id, example) values ($1, $2)',
        [idiom_id, example.example])
      // console.log(`added example`)
    } catch(e) {
      console.log(e)
    }
  }
}
queryExamples()

// add initiate_words

async function queryInitiateWords() {
  let examplesSheet = workbook.Sheets['initiate_words']
  let initiate_words: any[] = XLSX.utils.sheet_to_json(examplesSheet)
  for (let initiate_word of initiate_words) {
    try {
      let idiom_id = (await db.query(`SELECT id from idioms where key = $1`,[initiate_word.key])).rows[0].id
      // console.log(idiom_id)
      await db.query(
        'insert into initiate_words (idiom_id, initiate_words) values ($1, $2)',
        [idiom_id, initiate_word.initiate_words])
      // console.log(`added initiate_words`)
    } catch(e) {
      console.log(initiate_word.initiate_words, e)
    }
  }
}
queryInitiateWords()

// add origins

// async function queryOrigins() {
//   let originsSheet = workbook.Sheets['origins']
//   let origins: any[] = XLSX.utils.sheet_to_json(originsSheet)
//   for (let origin of origins) {
//     try {
//       let idiom_id = (await db.query(`SELECT id from idioms where key = $1`,[origin.key])).rows[0].id
//       // console.log(idiom_id)
//       await db.query(
//         'insert into origins (idiom_id, origin) values ($1, $2)',
//         [idiom_id, origin.origin])
//       // console.log(`added initiate_words`)
//     } catch(e) {
//       console.log(origin.origin, e)
//     }
//   }
// }
// queryOrigins()





// test only
// let str = ""
// let data: any[] = []
// let stu = [
//   {
//     name: 'Peter',
//     level: 25,
//     date_of_birth: '1995-05-15',
//     teacher_id: 1,
//   },
//   {
//     name: 'John',
//     level: 25,
//     date_of_birth: '1985-06-16',
//     teacher_id: 2,
//   },
//   {
//     name: 'Simon',
//     level: 25,
//     date_of_birth: '1980-07-17',
//     teacher_id: 3,
//   },
//   {
//     name: 'Simon',
//     level: 25,
//     date_of_birth: '1980-072-17',
//     teacher_id: 3,
//   },
// ]
// for (let i=1;i <= stu.length * 4; i++) {
//   if (i % 4 == 0) {
//     str += '$' + i + '), ';
//   } else if (i % 4 == 1) {
//     str += '($' + i + ',';
//   } else {
//     str += '$' + i + ',';
//   }
//   if (i <= stu.length) {
//     data.push(stu[i - 1].name)
//     data.push(stu[i - 1].level)
//     data.push(stu[i - 1].date_of_birth)
//     data.push(stu[i - 1].teacher_id)
//   }
// }
// str = str.slice(0, str.length - 2)
// let newStr = /* sql */ `INSERT INTO students
// (name,level,date_of_birth,teacher_id)
// VALUES ${str}`
// console.log(newStr,data)

// async function query() {
//   try {
//     await db.query(
//       newStr,
//       data,
//     )
//     console.log("success")
//   } catch (err) {
//     console.log(err)
//   }

// }

// query()




// add user_idiom_table

// async function queryUserIdiomTable() {
//   let situation_typesSheet = workbook.Sheets['situation_types']
//   let situation_types: any[] = XLSX.utils.sheet_to_json(situation_typesSheet)
//   for (let situation_type of situation_types) {
//     try {
//       let idiom_id = (await db.query(`SELECT id from idioms where key = $1`,[situation_type.key])).rows[0].id
//       console.log(idiom_id)
//       await db.query(
//         'insert into user_idiom_table (user_id, idiom_id) values ($1, $2)',
//         [1,idiom_id])
//       console.log(`added user_idiom_table`)
//     } catch(e) {
//       console.log(e)
//     }
//   }
// }
// queryUserIdiomTable()